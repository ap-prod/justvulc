/**
 * Returns action creator
 *
 * @export
 * @param {any} type
 * @returns {void}
 */
export default function createAction(type) {
  return payload => ({
    type,
    payload,
  });
}
