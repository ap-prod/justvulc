/**
 * formatting price
 *
 * @export
 * @param {number} n
 * @param {string} currency string
 * @returns {string}
 */
export default function (n, currency) {
  return `${currency} ${n.toFixed(2).replace(
    /./g,
    (c, i, a) => (i > 0 && c !== '.' && (a.length - i) % 3 === 0 ? `,${c}` : c),
  )}`;
}
