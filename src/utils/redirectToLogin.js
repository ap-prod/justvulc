/**
 * Redirect to login
 *
 * @export
 */
export default function () {
  window.location.replace(`${window.LOGIN_URL}${window.location.href}`);
}
