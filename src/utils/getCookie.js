/**
 * Return cookie value by name
 *
 * @export
 * @param {string} name
 * @returns {string| undefined}
 */
export default function (name) {
  const matches = document.cookie
    .match(new RegExp(`(?:^|; )${name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1')}=([^;]*)`)); // eslint-disable-line
  return matches ? decodeURIComponent(matches[1]) : undefined;
}
