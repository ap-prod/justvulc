import {
} from '../handlers'; // handlers

const initialValue = {
  items: [],
  loading: true,
};

const entries = (state = initialValue, { type, payload }) => { // eslint-disable-line
  switch (type) {
    case 'ENTRIES_FETCH_REQUESTED':
      return {
        ...state,
        items: [],
        loading: true,
      };

    case 'ENTRIES_FETCH_SUCCEEDED':
      return {
        ...state,
        items: payload,
        loading: false,
      };
    case 'REMOVE_ENTRY_REQUESTED':
      return {
        state,
        items: state
          .items
          .filter((item, i) => payload.index !== i),
      };

    default:
      return state;
  }
};

export default entries;
