import { call, put, takeEvery } from 'redux-saga/effects';
import { toastr } from 'react-redux-toastr';
import { Api } from '../services';

// worker Saga: will be fired on ENTRIES_FETCH_REQUESTED actions
function* fetchEntries(action) {
  try {
    const res = yield call(Api.fetchEntries, action.payload);
    yield put({ type: 'ENTRIES_FETCH_SUCCEEDED', payload: res.data });
  } catch (e) {
    yield put({ type: 'ENTRIES_FETCH_FAILED', error: e });
  }
}

// worker Saga: will be fired on REMOVE_ENTRY_REQUESTED actions
function* removeEntry(action) {
  try {
    const res = yield call(Api.removeEntry, action.payload.item);
    yield put({ type: 'REMOVE_ENTRY_SUCCEEDED', payload: res.data });
    toastr.success('Success', `Entry [${action.payload.item.documentNumber}] was deleted successfully!`);
  } catch (e) {
    yield put({ type: 'REMOVE_ENTRY_FAILED', error: e });
  }
}

function* entriesSaga() {
  yield takeEvery('ENTRIES_FETCH_REQUESTED', fetchEntries);
  yield takeEvery('REMOVE_ENTRY_REQUESTED', removeEntry);
}

export default entriesSaga;
