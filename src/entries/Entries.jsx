import React from 'react';
import { connect } from 'react-redux';
import { TableHeaderColumn } from 'react-bootstrap-table';
import PropTypes from 'prop-types';
import CircularProgress from 'material-ui/CircularProgress';
import RaisedButton from 'material-ui/RaisedButton';
import { DateTime } from 'luxon';
import { Helmet } from 'react-helmet';
import debounce from 'lodash/debounce';

import { editCard } from '../dashboard';
import {
  Table,
  ErrorBoundary,
  RangeDatePicker,
  EditCardModal,
  Clickable,
  Icon,
} from '../shared';
import { SearchBar } from './components';
import {
  Spinner,
  EntriesContainer,
  styles,
} from './styles';
import { fetchEntries, removeEntry } from './actions';

const COMPARATORS_MAP = [
  {
    sign: '<',
    value: 'CMP_LESS',
  },
  {
    sign: '>',
    value: 'CMP_GRT',
  },
  {
    sign: '<=',
    value: 'CMP_LESSEQU',
  },
  {
    sign: '>=',
    value: 'CMP_GRTEQU',
  },
  {
    sign: '=',
    value: 'CMP_EQU',
  },
  {
    sign: '!=',
    value: 'CMP_NOTEQU',
  },
];

class Entries extends React.Component {
  constructor(props) {
    super(props);

    this.getDateFilter = this.getDateFilter.bind(this);

    this.defaultFilter = {
      date: {
        min: DateTime.local().minus({ month: 6 }).toJSDate(),
        max: new Date(),
      },
      contragent: '',
      entry: '',
      documentNumber: '',
      description: '',
      summ: {
        number: 0,
        comparator: '',
      },
      fullText: '',
    };

    this.columns = [
      {
        propName: 'documentDate',
        title: 'Date',
        dataAlign: 'center',
        filter: {
          type: 'CustomFilter',
          getElement: this.getDateFilter,
        },
        formatter: d => this.getClickableTd(DateTime.fromSQL(d).toFormat('dd.MM.yyyy')),
        ref: React.createRef(),
      }, {
        propName: 'contragent',
        title: 'Contragent',
        dataAlign: 'center',
        formatter: d => this.getClickableTd(d),
        ref: React.createRef(),
      }, {
        propName: 'entry',
        title: 'Entry',
        dataAlign: 'center',
        formatter: d => this.getClickableTd(d),
        ref: React.createRef(),
      }, {
        propName: 'documentNumber',
        title: 'Document Number',
        dataAlign: 'center',
        formatter: d => this.getClickableTd(d),
        ref: React.createRef(),
      }, {
        propName: 'description',
        width: '700px',
        title: 'Description',
        dataAlign: 'center',
        formatter: d => this.getClickableTd(d, true),
        ref: React.createRef(),
      }, {
        propName: 'summ',
        title: 'Net Summ',
        dataAlign: 'right',
        filter: {
          type: 'NumberFilter',
          numberComparators: ['=', '>', '<'],
        },
        formatter: d => this.getClickableTd(d),
        ref: React.createRef(),
      },
    ];

    this.openStockWtab = this.openStockWtab.bind(this);
    this.clearFilters = this.clearFilters.bind(this);
    this.onFilterChange = this.onFilterChange.bind(this);
    this.getDateFilter = this.getDateFilter.bind(this);
    this.handleSearchSubmit = this.handleSearchSubmit.bind(this);
    this.handleRowClick = this.handleRowClick.bind(this);
  }

  componentDidMount() {
    this.props.dispatch(fetchEntries(this.defaultFilter));
    this.columns[0].ref.current.applyFilter(this.defaultFilter.date);
  }

  onFilterChange(filterObj) {
    const filter = {};
    const keys = Object.keys(filterObj);

    if (keys.length === 0) {
      this.props.dispatch(fetchEntries(this.defaultFilter));
      return;
    }

    keys.map((key) => { // eslint-disable-line
      switch (filterObj[key].type) {
        case 'NumberFilter':
          filter[key] = {
            number: +filterObj[key].value.number,
            comparator: COMPARATORS_MAP
              .filter(c => c.sign === filterObj[key].value.comparator)
              .map(c => c.value)[0],
          };
          break;
        case 'TextFilter':
          filter[key] = filterObj[key].value;
          break;
        default:
          filter.date = filterObj[key].value.callback();
          break;
      }
      if (filter.date) {
        const { min, max } = filter.date;
        if (min && !max) {
          filter.date = { min, max: this.defaultFilter.date.max };
        } else if (!min && max) {
          filter.date = { max, min: this.defaultFilter.date.min };
        }
      }

      this.props.dispatch(fetchEntries({
        ...this.defaultFilter,
        ...filter,
      }));
    });
  }

  getClickableTd(inner, hasTitle) {
    return <Clickable title={hasTitle ? inner : ''} >{inner}</Clickable>;
  }

  getDateFilter(filterHandler) {
    return (
      <RangeDatePicker
        onChange={this.dateFilter(filterHandler)}
        defaultValue={this.defaultFilter.date}
      />
    );
  }

  dateFilter(filterHandler) {
    return ({ min, max }) => {
      if (!min && !max) {
        filterHandler();
      } else {
        filterHandler({
          callback: () =>
            ({ min: DateTime.fromJSDate(min).toSQL(), max: DateTime.fromJSDate(max).toSQL() }),
        });
      }
    };
  }

  clearFilters() {
    this.columns.map((c) => {
      if (c.propName === 'documentDate') {
        c.ref.current.customFilter.props.onChange({ min: null, max: null });
      }
      return c.ref.current.cleanFiltered();
    });
  }

  handleRowClick(item, ...rest) {
    if (rest[2].target.innerText === 'DELETE') {
      this.props.dispatch(removeEntry({ item, index: rest[1] }));
    } else {
      this.openStockWtab(item);
    }
  }

  openStockWtab(item) {
    const cardProps = {
      item: {
        ...item,
        sourceColumn: {
          id: item.entryCode,
        },
      },
    };

    this.props.dispatch(editCard({ cardProps }));
  }

  handleSearchSubmit(searchStr) {
    // this.clearFilters();
    if (searchStr.length) {
      this.props.dispatch(fetchEntries({
        ...this.defaultFilter,
        fullText: searchStr,
      }));
    }
  }

  render() {
    const { entries: { items }, cardModalOptions } = this.props;
    return (
      <EntriesContainer>
        <Helmet>
          <title>JUSTVULC | Entries</title>
        </Helmet>
        <ErrorBoundary>
          <SearchBar onSearch={debounce(this.handleSearchSubmit, 400)} />
          <Table
            data={items}
            trClassName="strocked"
            pagination
            /* debounce prevent from  calling cb for  each filter */
            options={{
              onFilterChange: debounce(this.onFilterChange, 100),
              sizePerPage: 17,
              prePage: 'Prev',
              nextPage: 'Next',
              firstPage: 'First',
              lastPage: 'Last',
              sizePerPageList: [15, 25, 50],
              onRowClick: this.handleRowClick,
              noDataText: (
                <Spinner>
                  <CircularProgress size={120} thickness={10} />
                </Spinner>
              ),
              hideSizePerPage: true,
            }}
          >
            {
              this.columns.map((col, i) => (
                <TableHeaderColumn
                  ref={col.ref}
                  dataField={col.propName}
                  isKey={i === 0}
                  key={`subtable_${col.documentNumber}`}
                  headerAlign="center"
                  width={col.width || 'auto'}
                  dataAlign={col.dataAlign}
                  filter={col.filter || { type: 'TextFilter' }}
                  dataFormat={col.formatter}
                >
                  { col.title }
                </TableHeaderColumn>
              ))
            }
            <TableHeaderColumn
              editable={false}
              headerAlign="center"
              expandable={false}
              dataFormat={() => (
                <Icon
                  style={styles.removeIcon}
                  icon="close"
                  hoveredStyle={{
                    backgroundColor: 'rgb(187, 222, 251)',
                  }}
                  title="Remove"
                />
              )}
              dataAlign="center"
            >
              <RaisedButton onClick={this.clearFilters} label="Clear filters" />
            </TableHeaderColumn>
          </Table>
          {
            cardModalOptions.open && (
              <EditCardModal
                options={cardModalOptions}
              />
            )
          }
        </ErrorBoundary>
      </EntriesContainer>
    );
  }
}

Entries.propTypes = {
  dispatch: PropTypes.func.isRequired,
  entries: PropTypes.object.isRequired,
  cardModalOptions: PropTypes.object.isRequired,
};

Entries.defaultProps = {
};

const mapStateToProps = ({ entries, dashboard }) => (
  {
    entries,
    cardModalOptions: dashboard.cardModal,
  }
);

export default connect(
  mapStateToProps,
  null,
)((Entries));
