import React from 'react';
import PropTypes from 'prop-types';
import { SearchEntriesInput, SearchBarWrapper, ClearButton } from './styles';

class SearchBar extends React.Component {
  constructor() {
    super();
    this.state = { searchPhrase: '' };

    this.handleSearchPhraseChange = this.handleSearchPhraseChange.bind(this);
    this.handleClearButtonClick = this.handleClearButtonClick.bind(this);
    this.handleSearchInputKeyUp = this.handleSearchInputKeyUp.bind(this);
  }

  handleSearchPhraseChange(e) {
    this.setState({ searchPhrase: e.target.value });
    this.props.onSearch(this.state.searchPhrase);
  }

  handleClearButtonClick() {
    this.setState({ searchPhrase: '' });
  }

  handleSearchInputKeyUp(e) {
    if (e.which === 13) {
      this.props.onSearch(this.state.searchPhrase);
    }
  }

  render() {
    return (
      <SearchBarWrapper>
        <SearchEntriesInput
          floatingLabelText="Search"
          value={this.state.searchPhrase}
          onChange={this.handleSearchPhraseChange}
          onKeyUp={this.handleSearchInputKeyUp}
        />
        { !!this.state.searchPhrase.length &&
          <ClearButton onClick={this.handleClearButtonClick} >&#215;</ClearButton> }
      </SearchBarWrapper>
    );
  }
}

SearchBar.propTypes = {
  onSearch: PropTypes.func.isRequired,
};

export default SearchBar;
