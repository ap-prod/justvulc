import styled from 'styled-components';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';

export const SearchEntriesInput = styled(TextField)`
  width: 100% !important;
  margin-top: -20px;
`;

export const Button = styled(FlatButton)`
  margin-left: 20px;
`;

export const SearchBarWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  padding: 20px 0;
`;

export const ClearButton = styled.span`
  font-size: 40px;
  color: rgb(0, 188, 212);
  margin-left: -40px;
  display: block;
  height: 30px;
  width: 30px;
  cursor: pointer;
  z-index: 1;
  padding-bottom: 20px;
`;
