import { createAction } from '../../utils';

export const fetchEntries = createAction('ENTRIES_FETCH_REQUESTED');
export const removeEntry = createAction('REMOVE_ENTRY_REQUESTED');
