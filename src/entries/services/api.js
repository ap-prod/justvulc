import axios from 'axios';

export default class {
  static fetchEntries(filter) {
    if (process.env.NODE_ENV === 'production') {
      return axios.post(`${window.API_URL}/entries`, filter);
    }
    return axios.get('/entries.json');
  }

  static removeEntry({ entryCode }) {
    return axios.delete(`${window.API_URL}/delentry?id=${entryCode}`);
  }
}
