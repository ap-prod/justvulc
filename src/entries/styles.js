import styled from 'styled-components';

export const Spinner = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  height: 100%;
  justify-content: center;
`;

export const EntriesContainer = styled.div`
  margin: 5px 20px 20px;
`;

export const styles = {
  removeIcon: {
    width: '25px',
    padding: '3px',
    borderRadius: '50%',
    cursor: 'pointer',
    transition: '0.3s',
  },
};
