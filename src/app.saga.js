import { all } from 'redux-saga/effects';
import { dashboardSaga } from './dashboard';
import { entriesSaga } from './entries';
import { sharedSaga } from './shared';

export default function* appSaga() {
  yield all([
    dashboardSaga(),
    entriesSaga(),
    sharedSaga(),
  ]);
}
