import { toastr } from 'react-redux-toastr';

export const errorInterceptor = store => next => (action) => { // eslint-disable-line
  if (action.type.includes('FAIL')) {
    toastr.error('Error', 'Something went wrong!');
  }
  return next(action);
};
