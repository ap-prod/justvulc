import { combineReducers } from 'redux';
import { reducer as toastrReducer } from 'react-redux-toastr';
import { dashboard } from './dashboard';
import { entries } from './entries';

const trackingApp = combineReducers({
  dashboard,
  entries,
  toastr: toastrReducer,
});

export default trackingApp;
