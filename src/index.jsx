import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { getCookie, redirectToLogin } from './utils';

const date = new Date();
axios.get(`/config.json?${date.getTime()}`)
  .then(res => res.data)
  .then(({ API_URL, LOGIN_URL, DEV }) => {
    window.API_URL = API_URL;
    window.LOGIN_URL = LOGIN_URL;
    window.DEV = DEV;
    if (process.env.NODE_ENV === 'development' || getCookie(process.env.COOKIE_NAME)) {
      ReactDOM.render(<App />, document.getElementById('root'));
      registerServiceWorker();
    } else {
      redirectToLogin();
    }
  });
