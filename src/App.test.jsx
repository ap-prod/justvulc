import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

describe('App component', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
  });
  // it('should fail', () => {
  //   expect(1).toBe(2);
  // });
});
