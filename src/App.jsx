import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import ReduxToastr from 'react-redux-toastr';
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css';

import { Dashboard } from './dashboard';
import { Entries } from './entries';
import trackingAppReducers from './reducers';
import appSaga from './app.saga';
import { Page404 } from './shared/components';
import { Header } from './shared/containers';
import { errorInterceptor } from './middlewares';
import './App.css';

const sagaMiddleware = createSagaMiddleware();

const composeEnhancers =
  typeof window === 'object' &&
  /* eslint-disable no-underscore-dangle */
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
    /* eslint-enable */
    }) : compose;

const enhancer = composeEnhancers(applyMiddleware(sagaMiddleware, errorInterceptor));

const store = createStore(
  trackingAppReducers,
  enhancer,
);

const App = () => (
  <Provider store={store}>
    <MuiThemeProvider>
      <Router>
        <div>
          <Header />
          <Switch>
            <Redirect exact to="/dashboard" from="/" />
            <Route path="/dashboard" component={Dashboard} />
            <Route path="/entries" component={Entries} />
            <Route path="/contacts" component={Page404} />
            <Route path="/marketing" component={Page404} />
            <Route path="/inquiries" component={Page404} />
            <Route path="/deals" component={Page404} />
            <Route path="/reports" component={Page404} />
            <Route component={Page404} />
          </Switch>
          <ReduxToastr
            timeOut={4000}
            newestOnTop={false}
            preventDuplicates
            position="bottom-right"
            transitionIn="bounceIn"
            transitionOut="bounceOut"
            progressBar
          />
        </div>
      </Router>
    </MuiThemeProvider>
  </Provider>
);

sagaMiddleware.run(appSaga);

export default App;
