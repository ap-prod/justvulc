import axios from 'axios';

export default class {
  static fetchBoard() {
    if (process.env.NODE_ENV === 'production') {
      return axios.get(`${window.API_URL}/initdash`);
    }
    return axios.get('/data.json');
  }

  static fetchUser(payload) {
    return axios.get(`${window.API_URL}/getuser?cookie=${payload.cookie}`);
  }

  static logoutUser(payload) {
    return axios.get(`${window.API_URL}/logout?cookie=${payload.cookie}&login=${payload.login}`);
  }

  static fetchEntry(payload) {
    if (process.env.NODE_ENV === 'production') {
      return axios.get(`${window.API_URL}/getentry?id=${payload.item.id}${payload.entryType && `&entryType=${payload.entryType}`}`);
    }
    return axios.get('/modal-data.json');
  }

  static fetchWtab(payload) {
    if (process.env.NODE_ENV === 'production') {
      return axios.get(`${window.API_URL}/wtab?customer=${payload.item.idContragent}`);
    }
    return axios.get('/wtab.json');
  }

  static saveEditedCard(payload) {
    return axios.post(`${window.API_URL}/save`, payload);
  }

  static fetchMaterialsAndServices(payload) {
    if (process.env.NODE_ENV === 'production') {
      return axios.get(`${window.API_URL}/search?src=${payload.value}`);
    }
    return axios.get('/marerials-search.json');
  }

  static searchCustomer(payload) {
    if (process.env.NODE_ENV === 'production') {
      return axios.get(`${window.API_URL}/searchclient?src=${payload}`);
    }
    return axios.get('/customer-search.json');
  }

  static fetchStockWtab() {
    if (process.env.NODE_ENV === 'production') {
      return axios.get(`${process.env.API_URL}/stock`);
    }
    return axios.get('/stock-wtab.json');
  }
}
