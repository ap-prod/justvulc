
export function openEntryModal(state, payload) {
  return {
    ...state,
    cardModal: {
      ...state.cardModal,
      loading: false,
      data: payload,
    },
  };
}

export function openEntriesModal(state, payload) {
  if (payload.length === 1) {
    return openEntryModal(state, payload[0]);
  }

  const newCustomerData =
    payload
      .map(({ customerData }) => customerData)
      .reduce((prev, next) => ({
        ...prev,
        customerId: matchAndConcatStrings(prev.customerId, next.customerId),
        notes: matchAndConcatStrings(prev.notes, next.notes),
        packages: matchAndConcatStrings(prev.packages, next.packages),
        reference: matchAndConcatStrings(prev.reference, next.reference),
        documentNumber: matchAndConcatStrings(prev.documentNumber, next.documentNumber),
        volume: matchAndConcatStrings(prev.volume, next.volume),
        weight: matchAndConcatStrings(prev.weight, next.weight),
      }));
  const newMaterialsAndServices =
    payload
      .map(({ materialsAndServices }) => materialsAndServices)
      .reduce((prev, next) => prev.concat(next));
  const summaraizedMaterialsAndServices = newMaterialsAndServices.map((material) => {
    const dupl = newMaterialsAndServices.filter((m) => {
      const equalityArr = Object.keys(m).map(k => ((k !== 'qty' || k !== 'description') ? m[k] === material[k] : true));
      return !equalityArr.includes(false);
    });
    if (dupl) {
      return dupl.reduce((curr, next) => ({
        ...curr,
        qty: curr.qty + next.qty,
      }));
    }
    return material;
  });
  return {
    ...state,
    cardModal: {
      ...state.cardModal,
      loading: false,
      data: {
        ...payload[0],
        customerData: newCustomerData,
        materialsAndServices: summaraizedMaterialsAndServices,
        entries: payload,
      },
    },
  };
}

function matchAndConcatStrings(strA, strB) {
  if (!strA && !strB) {
    return '';
  }
  if (!strA && strB) {
    return strB;
  }
  if (strA && !strB) {
    return strA;
  }
  return !strA.includes(strB) ? strA : strA.concat(` + ${strB}`);
}
