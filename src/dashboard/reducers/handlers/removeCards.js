import { defaultColumnProps } from '../';

export function removeGroupOfCardsFromColumn(items, checkedCards) {
  return items.filter(item => !checkedCards.filter(c => c === item.id).length);
}

export function removeCards(state, payload) {
  // TODO: uncomment following code for allow user delete group of cards
  // const { sourceColumn } = payload.item;

  // if (!sourceColumn.checkedCards.length) {
  return removeSingleCardToAnotherColumn(state, payload);
  // }
  // const columns = removeGroupOfCards(state, sourceColumn);

  // return {
  //   ...state,
  //   columns,
  // };
}

export function removeSingleCardToAnotherColumn(state, payload) {
  return {
    ...state,
    columns: state.columns.map(col => (
      {
        ...col,
        items: col.items === null ? [] : col.items.filter(item => item.id !== payload.item.id),
      }
    )),
  };
}

export function removeGroupOfCards(state, sourceColumn) {
  return state.columns.map((col) => {
    let { items } = col;
    // remove dragged items from source column
    if (col.id === sourceColumn.id) {
      items = removeGroupOfCardsFromColumn(col.items, sourceColumn.checkedCards);
    }

    return {
      ...col,
      ...defaultColumnProps,
      items,
    };
  });
}
