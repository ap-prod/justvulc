import {
  defaultColumnProps,
  removeGroupOfCardsFromColumn,
} from '../';

export function moveCards(state, payload) {
  const { sourceColumn } = payload.item;
  // if user move single card
  // and don't use checkboxes
  if (!sourceColumn.checkedCards.length) {
    return moveSingleCardToAnotherColumn(state, payload);
  }
  // map new column list
  return moveGroupOfCards(state, sourceColumn, payload);
}

export function moveGroupOfCards(state, sourceColumn, payload) {
  const { targetColumn } = payload;
  // find items, which are checked
  const movedItems = sourceColumn.items.filter(item => (
    !!sourceColumn.checkedCards.filter(c => c === item.id).length
  ));

  const columns = state.columns.map((col) => {
    let { items } = col;
    // remove dragged items from source column
    if (col.id === sourceColumn.id) {
      items = removeGroupOfCardsFromColumn(col.items, sourceColumn.checkedCards);
    }

    // add dragged items to target column
    if (col.id === targetColumn.id) {
      // replace null from BE with empty array
      items = items === null ? [] : items;
      items.push(...movedItems);
    }

    return {
      ...col,
      ...defaultColumnProps,
      items,
    };
  });

  return {
    ...state,
    cardModal: {
      ...state.cardModal,
      title: `${payload.item.name}, [ ${payload.item.sourceColumn.name} ➡ ${payload.targetColumn.name} ]`,
      open: true,
      move: true,
      item: payload.item,
      loading: true,
      targetColumn: payload.targetColumn,
    },
    columns,
  };
}

export function moveSingleCardToAnotherColumn(state, payload) {
  return {
    ...state,
    cardModal: {
      ...state.cardModal,
      title: `${payload.item.name}, [ ${payload.item.sourceColumn.name} ➡ ${payload.targetColumn.name} ]`,
      open: true,
      move: true,
      item: payload.item,
      loading: true,
      targetColumn: payload.targetColumn,
    },
    columns: state.columns.map(col => (
      {
        ...col,
        items: col.items !== null ? col.items.filter(item => item.id !== payload.item.id) : [],
      }
    )).map((col) => {
      if (col.id === payload.targetColumn.id) {
        col.items.push(payload.item);
      }
      return col;
    }),
  };
}
