import {
  dashboard,
  stateMock,
} from './';

describe('dashboard reducer', () => {
  it('should return state', () => {
    expect(dashboard(stateMock, { action: 'ADD_ITEM' })).toEqual(stateMock);
  });
});
