export default {
  open: false,
  update: false,
  move: false,
  title: '',
  loading: false,
  create: false,
  data: null,
  seachedMaterials: [],
  searchedCustomers: [],
};
