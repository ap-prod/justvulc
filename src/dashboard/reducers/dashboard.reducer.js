import {
  moveCards,
  removeCards,
  openEntryModal,
  openEntriesModal,
} from './';

import { defaultColumnProps, cardModalInitState, cardModalInitDataState, wtabModalInitState } from './defaults';

const dashboard = (state = {
  columns: [],
  unautoraized: true,
  needRefreshBoardData: false,
  cardModal: cardModalInitState,
  wtabModal: wtabModalInitState,
}, { type, payload }) => {
  switch (type) {
    case 'ADD_CARD':
      return state;

    case 'REMOVE_CARD':
      return removeCards(state, payload);

    case 'MOVE_CARDS':
      return moveCards(state, payload);

    case 'CHECK_CARD':
      return {
        ...state,
        columns: state.columns.map((col) => {
          if (col.id === payload.columnId) {
            return { ...col, ...payload.checkData };
          }
          return {
            ...col,
            ...defaultColumnProps,
          };
        }),
      };

    case 'BOARD_DATA_FETCH_SUCCEEDED':
      return {
        ...state,
        needRefreshBoardData: false,
        columns: payload.map(c => (
          {
            ...c,
            ...defaultColumnProps,
          })),
      };

    case 'USER_DATA_FETCH_SUCCEEDED':
      return {
        ...state,
        unautoraized: payload.id === 0 && payload.name === null,
        user: payload,
      };

    case 'ENTRY_DATA_FETCH_SUCCEEDED':
      return openEntryModal(state, payload);

    case 'ENTRIES_DATA_FETCH_SUCCEEDED':
      return openEntriesModal(state, payload);

    case 'MOVE_ENTRY_DATA_FETCH_SUCCEEDED':
      return {
        ...state,
        cardModal: {
          ...state.cardModal,
          loading: false,
          data: payload,
        },
      };

    case 'LOGOUT_SUCCEEDED':
      return {
        ...state,
        unautoraized: true,
      };

    case 'OPEN_WTAB_CARD_MODAL':
      return {
        ...state,
        wtabModal: {
          open: true,
          loading: true,
          item: payload.item,
          stock: false,
        },
      };

    case 'OPEN_STOCK_WTAB_CARD_MODAL':
      return {
        ...state,
        wtabModal: {
          open: true,
          loading: true,
          item: payload.item,
          stock: true,
        },
      };

    case 'CLOSE_WTAB_CARD_MODAL':
      return {
        ...state,
        wtabModal: {
          ...wtabModalInitState,
        },
      };

    case 'WTAB_DATA_FETCH_SUCCEEDED':
      return {
        ...state,
        wtabModal: {
          ...state.wtabModal,
          loading: false,
          data: payload,
        },
      };

    case 'STOCK_WTAB_DATA_FETCH_SUCCEEDED':
      return {
        ...state,
        wtabModal: {
          ...state.wtabModal,
          loading: false,
          data: payload,
        },
      };

    case 'OPEN_EDIT_CARD_MODAL':
      return {
        ...state,
        cardModal: {
          ...state.cardModal,
          item: payload.cardProps.item,
          title: `Update ${payload.cardProps.item.name || payload.cardProps.item.contragent} [ ${payload.cardProps.item.sourceColumn && payload.cardProps.item.sourceColumn.name} ]`,
          update: true,
          loading: true,
          open: true,
        },
      };

    case 'OPEN_CREATE_CARD_MODAL':
      return {
        ...state,
        cardModal: {
          ...cardModalInitState,
          data: cardModalInitDataState,
          item: payload.cardProps.item,
          title: `Add Card [ ${payload.cardProps.item.sourceColumn.name} ]`,
          create: true,
          loading: true,
          open: true,
        },
      };

    case 'CLOSE_CREATE_CARD_MODAL':
      return {
        ...state,
        cardModal: {
          ...cardModalInitState,
        },
      };

    case 'SAVE_EDITED_CARD_SUCCEEDED':
      return {
        ...state,
        cardModal: {
          ...cardModalInitState,
        },
      };

    case 'SAVE_MOVED_CARD_SUCCEEDED':
      return {
        ...state,
        columns: [],
        needRefreshBoardData: true,
        cardModal: {
          ...cardModalInitState,
        },
      };

    case 'CREATE_CARD_SUCCEEDED':
      return {
        ...state,
        columns: [],
        needRefreshBoardData: true,
        cardModal: {
          ...cardModalInitState,
        },
      };

    case 'CHANGE_CUSTOMER_DATA':
      return {
        ...state,
        cardModal: {
          ...state.cardModal,
          data: {
            ...state.cardModal.data,
            customerData: payload.customerData,
          },
        },
      };

    case 'ITEMS_BY_ART_NUMBER_FETCH_SUCCEEDED':
      return {
        ...state,
        cardModal: {
          ...state.cardModal,
          seachedMaterials: payload,
        },
      };

    case 'CUSTOMERS_SEARCH_FETCH_SUCCEEDED':
      return {
        ...state,
        cardModal: {
          ...state.cardModal,
          searchedCustomers: payload,
        },
      };

    case 'CLEAR_SEARCHED_CUSTOMERS':
      return {
        ...state,
        cardModal: {
          ...state.cardModal,
          searchedCustomers: [],
        },
      };

    case 'ADD_MATERIAL_AND_SERVICES_ROW':
      return {
        ...state,
        cardModal: {
          ...state.cardModal,
          data: {
            ...state.cardModal.data,
            materialsAndServices: [...state.cardModal.data.materialsAndServices, payload.row],
          },
        },
      };

    case 'EDIT_MATERIAL_AND_SERVICES_ROW':
      return {
        ...state,
        cardModal: {
          ...state.cardModal,
          data: {
            ...state.cardModal.data,
            materialsAndServices:
              state
                .cardModal
                .data
                .materialsAndServices
                .map((row, i) => (payload.rowIndex === i ? payload.row : row)),
          },
        },
      };

    case 'REMOVE_MATERIAL_AND_SERVICES_ROW':
      return {
        ...state,
        cardModal: {
          ...state.cardModal,
          data: {
            ...state.cardModal.data,
            materialsAndServices:
              state.cardModal.data.materialsAndServices
                .filter(r => payload.row.id !== r.id),
          },
        },
      };

    default:
      return state;
  }
};

export default dashboard;
