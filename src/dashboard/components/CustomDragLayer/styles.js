import styled from 'styled-components';

export const Layer = styled.div`
  width: ${props => `calc(${props.w}% - 14px)`};
  position: fixed;
  pointer-events: none;
  z-index: 2000;
  left: 0;
  top: 0;
  padding: 0 20px;
  height: 100%;
`;

export const Item = styled.div`
  ${props => !props.currentOffset && 'display: none'};
  ${props => props.currentOffset &&
    `transform: translate(${props.currentOffset.x}px, ${props.currentOffset.y}px)`};
  opacity: .8;
`;
