import React from 'react';
import PropTypes from 'prop-types';
import { DragLayer } from 'react-dnd';
import { ColumnCard } from '../';
import { shouldPureComponentUpdate } from '../../../utils';
import { Layer, Item } from './styles';

function collect(monitor) {
  return {
    item: monitor.getItem(),
    initialOffset: monitor.getInitialSourceClientOffset(),
    currentOffset: monitor.getSourceClientOffset(),
    isDragging: monitor.isDragging(),
  };
}
// eslint-disable-next-line
class CustomDragLayer extends React.Component {
  getListOfCards(items, checkedCards) {
    const draggedItems = items.filter(i =>
      checkedCards.filter(c => c === i.id).length > 0);
    return draggedItems.map(item => (
      <ColumnCard
        key={`drag_item_${item.id}`}
        isOpen={false}
        isChecked
        {...{ item }}
      />));
  }

  render() {
    const {
      item,
      isDragging,
      width,
      currentOffset,
    } = this.props;
    if (!isDragging) {
      return null;
    }
    return (
      <Layer w={width}>
        <Item currentOffset={currentOffset}>
          {
            item.item.sourceColumn.checkedCards.length < 2 ?
              <ColumnCard
                isOpen
                isChecked={
                  item.item.sourceColumn.checkedCards.filter(c => c === item.item.id).length > 0
                }
                {...item}
              />
            :
              this.getListOfCards(item.item.sourceColumn.items, item.item.sourceColumn.checkedCards)
          }
        </Item>
      </Layer>
    );
  }
}

CustomDragLayer.propTypes = {
  item: PropTypes.object,
  isDragging: PropTypes.bool.isRequired,
  width: PropTypes.number.isRequired,
  currentOffset: PropTypes.object,
};

CustomDragLayer.defaultProps = {
  item: null,
  currentOffset: null,
};

CustomDragLayer.shouldComponentUpdate = shouldPureComponentUpdate;

export default DragLayer(collect)(CustomDragLayer);
