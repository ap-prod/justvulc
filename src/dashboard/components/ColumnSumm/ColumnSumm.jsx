import React from 'react';
import PropTypes from 'prop-types';
import { formatMoney } from '../../../utils';

const ColumnSumm = ({ style, summ }) => {
  if (summ !== null) {
    return [
      <span
        style={style}
        key={`summ_amount_${summ}`}
      >
        { formatMoney(summ, '€') }
      </span>,
    ];
  }
  return null;
};

ColumnSumm.propTypes = {
  style: PropTypes.object,
  summ: PropTypes.number,
};

export default ColumnSumm;
