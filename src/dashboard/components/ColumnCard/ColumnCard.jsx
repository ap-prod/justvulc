import React from 'react';
import PropTypes from 'prop-types';
import { DateTime } from 'luxon';
import {
  CardHeader,
  PriceLabel,
  ProcessingDurationLabel,
  Name,
  EditCardBtn,
  Container,
  StyledCardText,
  StyledCheckbox,
} from './styles';
import { formatMoney } from '../../../utils';

const COLORS_MAP = {
  green: '#C8E6C9',
  yellow: '#FFF59D',
  red: '#EF9A9A',
  gray: '#E0E0E0',
};

class ColumnCard extends React.Component {
  constructor({ isOpen, item }) {
    super();
    this.state = {
      isCollapsed: typeof isOpen === 'boolean' ? !isOpen : item.color === 'gray',
    };
    this.handleHeaderClick = this.handleHeaderClick.bind(this);
    this.handleDblClick = this.handleDblClick.bind(this);
    this.handleCheck = this.handleCheck.bind(this);
    this.handleMouseLeave = this.handleMouseLeave.bind(this);
    this.handleMouseEnter = this.handleMouseEnter.bind(this);
    this.handleCardEditBtnClick = this.handleCardEditBtnClick.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { isCollapsed, hovered } = this.state;
    const { disabledCheckbox, isChecked, isOpen } = this.props;
    return isCollapsed !== nextState.isCollapsed ||
      disabledCheckbox !== nextProps.disabledCheckbox ||
      isOpen !== nextProps.isOpen ||
      isChecked !== nextProps.isChecked ||
      hovered !== nextState.hovered;
  }

  handleHeaderClick() {
    this.setState({
      isCollapsed: !this.state.isCollapsed,
    });
  }

  handleDblClick() {
    this.props.onDoubleClick(this.props.item);
  }

  handleCheck(e, value) {
    this.props.onCardCheck(this.props.item, value);
  }

  handleMouseHover(isMouseEnter) {
    this.setState({
      ...this.state,
      hovered: isMouseEnter,
    });
  }

  handleMouseEnter() {
    this.handleMouseHover(true);
  }

  handleMouseLeave() {
    this.handleMouseHover(false);
  }

  handleCardEditBtnClick() {
    this.props.onEditCardBtnClick();
  }

  render() {
    const {
      item,
      disabledCheckbox,
      isChecked,
    } = this.props;
    return (
      <Container
        bsh={`${COLORS_MAP[item.color]} -1px 1px 5px, ${COLORS_MAP[item.color]} 1px 0px 2px`}
        pb={this.state.isCollapsed ? '0px' : '3px'}
        onMouseEnter={this.handleMouseEnter}
        onMouseLeave={this.handleMouseLeave}
        onDoubleClick={this.handleDblClick}
      >
        <CardHeader
          bgc={COLORS_MAP[item.color]}
          cursor={this.state.isCollapsed ? 'pointer' : 'move'}
          title={item.name}
          onClick={this.handleHeaderClick}
        >
          <StyledCheckbox
            checked={isChecked}
            iconStyle={{ width: '15px' }}
            onCheck={this.handleCheck}
            disabled={disabledCheckbox}
            onClick={e => e.stopPropagation()}
          />
          <Name>{item.name}</Name>
        </CardHeader>
        {
          this.state.hovered && (
            <EditCardBtn
              onClick={this.handleCardEditBtnClick}
            >
              E
            </EditCardBtn>
          )
        }
        <StyledCardText dpl={this.state.isCollapsed ? 'none' : 'block'}>
          <PriceLabel>
            {formatMoney(item.amountVAT, '€')}
          </PriceLabel>
          <PriceLabel>
            { DateTime.fromSQL(item.date).toFormat('dd.MM.yy') }
          </PriceLabel>
          <ProcessingDurationLabel>{item.processingDuration}</ProcessingDurationLabel>
        </StyledCardText>
      </Container>
    );
  }
}

ColumnCard.propTypes = {
  isChecked: PropTypes.bool.isRequired,
  item: PropTypes.shape({
    name: PropTypes.string,
    amount: PropTypes.number,
    processingDuration: PropTypes.string,
  }).isRequired,
  isOpen: PropTypes.bool,
  onCardCheck: PropTypes.func,
  disabledCheckbox: PropTypes.bool,
  onEditCardBtnClick: PropTypes.func,
  onDoubleClick: PropTypes.func,
};

ColumnCard.defaultProps = {
  isOpen: undefined,
  onCardCheck: null,
  disabledCheckbox: false,
  onEditCardBtnClick: null,
  onDoubleClick: null,
};

export default ColumnCard;
