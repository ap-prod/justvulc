import styled from 'styled-components';
import { CardText } from 'material-ui/Card';
import Checkbox from 'material-ui/Checkbox';

export const StyledCardText = styled(CardText)`
  display: ${props => props.dpl} !important;
  padding: 0px !important;
  font-size: 12px !important;
  padding: 0 !important;
`;

export const StyledCheckbox = styled(Checkbox)`
  display: block !important;
  width: 16px !important;
  padding-right: 4px !important;
`;

export const Label = styled.label`
  display: flex;
  align-items: center;
  font-family: Roboto, sans-serif;
`;

export const PriceLabel = Label.extend`
  margin: 5px;
`;

export const ProcessingDurationLabel = Label.extend`
  margin-right: 5px;
  justify-content: flex-end;
  opacity: 0.7;
`;

export const CardHeader = styled.div`
  color: rgba(0, 0, 0, 0.5);
  white-space: nowrap;
  overflow: hidden;
  width: 100%;
  text-overflow: ellipsis;
  font-size: 12px;
  padding: 5px;
  line-height: 12px;
  box-sizing: border-box;
  outline: 0;
  display: flex;
  font-family: Roboto, sans-serif;
  align-items: center;
  background-color: ${props => props.bgc};
  cursor: ${props => props.cursor};
`;

export const Name = styled.div`
  white-space: nowrap;
  overflow: hidden;
  font-family: Roboto, sans-serif;
  width: 100%;
  text-overflow: ellipsis;
`;

export const Container = styled.div`
  margin-bottom: 5px;
  background-color: #fff;
  transition: initial;
  box-shadow: ${props => props.bsh};
  padding-bottom: ${props => props.pb};
`;

export const EditCardBtn = styled.div`
    height: 20px;
    width: 20px;
    border-radius: 3px;
    display: flex;
    background-color: #fff;
    float: right;
    margin-top: -28px;
    z-index: 1;
    position: relative;
    margin-right: 5px;
    cursor: pointer;
    color: rgba(0, 0, 0, 0.5);
    justify-content: center;
    align-items: center;
    font-size: 12px;
    border: 1px solid rgb(0, 188, 212);
`;
