import React from 'react';
import { Card } from 'material-ui/Card';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const CardPlaceholder = ({ className }) =>
  <Card className={className} />;

CardPlaceholder.propTypes = {
  className: PropTypes.string.isRequired,
};

const StyledCardPlaceholder = styled(CardPlaceholder)`
  border: 2px dashed rgb(0, 188, 212);
  min-height: 82px;
`;

export default StyledCardPlaceholder;
