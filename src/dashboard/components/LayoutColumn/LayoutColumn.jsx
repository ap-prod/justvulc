import React from 'react';
import PropTypes from 'prop-types';
import { DateTime } from 'luxon';
import { DropTarget } from 'react-dnd';

import Trash from '../Trash';
import { formatMoney } from '../../../utils';
import {
  CardPlaceholder,
  ColumnSumm,
} from '../';
import { ColumnCardsList } from '../../containers';
import {
  StockCard,
  StockHeader,
  PriceLabel,
  ProcessingDurationLabel,
  ColumnSummary,
  EditCardBtn,
  ColumnLayout,
  ColumnBody,
  ColunmHeader,
  ColumnFooter,
} from './styles';

/**
 * Config target column CBs
 */
const columnTarget = {
  drop({ column }) {
    return {
      column,
      move: true,
    };
  },
  canDrop({ column }, monitor) {
    const { item } = monitor.getItem();
    return column
      .dropAllowedColumnIds
      .filter(id => id === item.sourceColumn.id)
      .length > 0;
  },
};

/**
 *
 * @param {*} connect
 * @param {*} monior
 */
function collect(conn, monitor) {
  return {
    connectDropTarget: conn.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    item: monitor.getItem(),
  };
}

/**
 * Column layout
 */
class LayoutColumn extends React.Component {
  constructor(props) {
    super(props);

    this.state = { hovered: false };
    this.handleMouseLeave = this.handleMouseLeave.bind(this);
    this.handleMouseEnter = this.handleMouseEnter.bind(this);
    this.handleAddCardBtnClick = this.handleAddCardBtnClick.bind(this);
    this.handleStockWtabDoubleClick = this.handleStockWtabDoubleClick.bind(this);
  }

  getWinStockCard(stock) {
    return (
      <React.Fragment>
        <StockCard onDoubleClick={this.handleStockWtabDoubleClick} >
          <StockHeader>STOCK</StockHeader>
          <PriceLabel>{formatMoney(stock.amountVAT, '€')}</PriceLabel>
          <PriceLabel>{ DateTime.fromSQL(stock.date).toFormat('dd.MM.yy') }</PriceLabel>
          <ProcessingDurationLabel>{stock.processingDuration}</ProcessingDurationLabel>
        </StockCard>
      </React.Fragment>
    );
  }

  handleMouseHover(isMouseEnter) {
    this.setState({
      ...this.state,
      hovered: isMouseEnter,
    });
  }

  handleMouseEnter() {
    this.handleMouseHover(true);
  }

  handleMouseLeave() {
    this.handleMouseHover(false);
  }

  handleAddCardBtnClick() {
    this.props.onAddCardBtnClick(this.props.column);
  }

  handleStockWtabDoubleClick() {
    this.props.onStockWtabDoubleClick(this.props.items[0]);
  }

  render() {
    const {
      items,
      maxWidth,
      minWidth,
      width,
      name,
      connectDropTarget,
      isOver,
      canDrop,
      item,
      column,
    } = this.props;

    return (
      <React.Fragment>
        { item !== null && <Trash /> }
        <ColumnLayout
          innerRef={instance => connectDropTarget(instance)}
          maxWidth={maxWidth !== 'auto' ? `${maxWidth}px` : maxWidth}
          minWidth={minWidth !== 'auto' ? `${minWidth}px` : minWidth}
          w={`${width}%`}
          opacity={item !== null && !canDrop ? '0.5' : '1'}
          onMouseEnter={this.handleMouseEnter}
          onMouseLeave={this.handleMouseLeave}
        >
          <ColunmHeader>
            {name}
            {
              this.state.hovered && (
                <EditCardBtn onClick={this.handleAddCardBtnClick} >+</EditCardBtn>
              )
            }
          </ColunmHeader>
          <ColumnBody>
            <ColumnCardsList
              items={items === null || column.name === 'WIN' ? [] : items}
              column={column}
            />
            {
              (isOver && canDrop) &&
              <CardPlaceholder />
            }
          </ColumnBody>
          {(column.name === 'WIN' && items && items[0]) && this.getWinStockCard(items[0])}
          <ColumnFooter>
            <ColumnSummary>
              <ColumnSumm
                summ={
                  (items !== null && items.length) ?
                  items.reduce((prev, curr) =>
                    ({ ...prev, amount: prev.amount + curr.amount }))
                    .amount
                  :
                  0}
              />
            </ColumnSummary>
          </ColumnFooter>
        </ColumnLayout>
      </React.Fragment>
    );
  }
}

LayoutColumn.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    processingDuration: PropTypes.string,
    amount: PropTypes.number,
    id: PropTypes.number,
  })),
  maxWidth: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  minWidth: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  width: PropTypes.number.isRequired,
  connectDropTarget: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  isOver: PropTypes.bool.isRequired,
  canDrop: PropTypes.bool.isRequired,
  item: PropTypes.object,
  column: PropTypes.object.isRequired,
  onAddCardBtnClick: PropTypes.func.isRequired,
  onStockWtabDoubleClick: PropTypes.func.isRequired,
};

LayoutColumn.defaultProps = {
  maxWidth: 'auto',
  minWidth: 'auto',
  items: null,
  item: null,
};

export default DropTarget('card', columnTarget, collect)(LayoutColumn);
