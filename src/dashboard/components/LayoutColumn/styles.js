import styled, { keyframes } from 'styled-components';

const blink = keyframes`
  10% {
    border-top: 3px solid #EF5350;
  }
  20% {
    border-right: 3px solid #EF5350;
  }
  30% {
    border-bottom: 3px solid #EF5350;
  }
  40% {
    border-left: 3px solid #EF5350;
  }
`;

export const TrashWrapper = styled.div`
  border-radius: 3px;
  border: 3px solid #EF5350;
  background-color: #FFEBEE;
  display: flex;
  min-height: 70px;
  justify-content: center;
  width: calc(100% - 30px);
  padding: 20px 0;
  margin: 15px;
  box-sizing: padding-box;
  position: fixed;
  bottom: -5px;
  width: 150px;
  z-index: 1999;
  right: -5px;
  &:hover {
    animation: ${blink} 2s ease-in-out infinite;
    border: 3px solid transparent;
  }
`;

export const ColumnLayout = styled.div`
  height: 98%;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  margin: 10px 6px 10px 0;
  box-sizing: border-box;
  font-family: Roboto, sans-serif;
  box-shadow: rgba(0, 0, 0, 0.12) 1px 1px 1px 3px, rgba(0, 0, 0, 0.12) 1px 1px 1px 1px;
  border-radius: 2px;
  overflow-x: hidden;
  max-width: ${props => props.maxWidth};
  min-width: ${props => props.minWidth};
  width: ${props => props.w};
  opacity: ${props => props.opacity};
`;

export const ColumnBody = styled.div`
  padding: 4px;
  overflow-y: auto;
  overflow-x: hidden;
  height: 100%;
  display: flex;
  flex-direction: column;
`;

export const ColunmHeader = styled.div`
  width: 100%;
  height: 30px;
  background-color: #BBDEFB;
  line-height: 30px;
  text-align: center;
  min-height: 30px;
  font-size: 13px;
  font-family: Roboto, sans-serif;
  position: relative;
  text-overflow: ellipsis;
  white-space: nowrap;
  border-radius: 2px;
  overflow: hidden;
  color: rgba(0, 0, 0, 0.87);
`;

export const ColumnFooter = styled.div`
  margin-top: auto;
  padding: 10px;
  display: flex;
  flex-direction: row;
  align-items: center;
  background-color: #B0BEC5;
  min-height: 33px;
  box-sizing: border-box;
`;

export const EditCardBtn = styled.div`
  height: 20px;
  width: 20px;
  border-radius: 3px;
  display: flex;
  background-color: #fff;
  right: 0;
  top: 0;
  margin-top: 3px;
  z-index: 1;
  position: absolute;
  margin-right: 5px;
  cursor: pointer;
  color: rgb(0, 188, 212, 0.7);
  justify-content: center;
  align-items: center;
  font-size: 20px;
  line-height: 20px;
  font-weight: bold;
  border: 1px solid rgb(0, 188, 212);
`;

export const ColumnSummary = styled.div`
  padding-left: 10px;
  font-size: 12px;
`;

export const StockCard = styled.div`
  background-color: #FFFDE7;
  padding: 5px;
  font-size: 12px;
  box-shadow: #FBC02D -1px 1px 5px, #FBC02D 1px 0px 2px;
  margin: 5px;
  cursor: pointer;
`;

export const StockHeader = styled.div`
  background-color: #FFF176;
  text-align: center;
  margin: -5px -5px 0 -5px;
  padding: 5px;
`;

export const PriceLabel = styled.div`
  display: flex;
  align-items: center;
  font-family: Roboto, sans-serif;
  margin: 5px;
`;

export const ProcessingDurationLabel = styled.div`
  display: flex;
  align-items: center;
  font-family: Roboto, sans-serif;
  margin-right: 5px;
  justify-content: flex-end;
  opacity: 0.7;
`;
