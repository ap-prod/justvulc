import React from 'react';
import { DropTarget } from 'react-dnd';
import styled, { keyframes } from 'styled-components';
import { Icon } from '../../../shared';

const blink = keyframes`
  10% {
    border-top: 3px solid #EF5350;
    background-color: #FFEBEE;
  }
  20% {
    border-right: 3px solid #EF5350;
    background-color: #FFCDD2;
  }
  30% {
    border-bottom: 3px solid #EF5350;
    background-color: #FFEBEE;
  }
  40% {
    border-left: 3px solid #EF5350;
    background-color: #FFCDD2;
  }
`;

export const TrashWrapper = styled.div`
  border-radius: 3px;
  border: 3px solid #FCE4EC;
  background-color: #FFEBEE;
  display: flex;
  height: 70px;
  justify-content: center;
  width: calc(100% - 30px);
  padding: 20px 0;
  margin: 15px;
  box-sizing: padding-box;
  position: fixed;
  bottom: -5px;
  width: 120px;
  z-index: 1999;
  tranition: .4s;
  right: -5px;
  overflow: hidden;
  ${props => (props.isOver &&
    `animation: ${blink} 2s ease-in-out infinite;`
  )}
`;

/**
 * Config target column CBs
 */
const TrashTarget = {
  drop: () => ({ remove: true }),
};

/**
 * Collect function
 *
 * @param {*} connect
 * @param {*} monior
 */
function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
  };
}

/**
 * Thrash
 */
const Trash = ({
  connectDropTarget, // eslint-disable-line
  isOver, // eslint-disable-line
}) => (
  <TrashWrapper innerRef={instance => connectDropTarget(instance)} isOver={isOver}>
    <Icon
      style={{ height: '60px' }}
      icon="bin"
    />
  </TrashWrapper>
);

export default DropTarget('card', TrashTarget, collect)(Trash);
