import React from 'react';
import { connect } from 'react-redux';
import CircularProgress from 'material-ui/CircularProgress';
import HTML5Backend from 'react-dnd-html5-backend';
import { DragDropContext } from 'react-dnd';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import {
  fetchBoardData,
  openCreateCardModal,
} from './actions';
import {
  UnscrollableColumnsLayout,
  ErrorBoundary,
  openStockWtabCardModal,
  EditCardModal,
} from '../shared';
import {
  LayoutColumn,
  CustomDragLayer,
} from './components';

import { WtabCardModal } from './containers';

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.onAddCard = this.onAddCard.bind(this);
    this.openStockWtab = this.openStockWtab.bind(this);
  }

  /**
   * Set document title when component mounted
   */
  componentDidMount() {
    this.fetchBoardData();
  }

  componentDidUpdate() {
    if (this.props.needRefreshBoardData) {
      this.fetchBoardData();
    }
  }

  onAddCard(column) {
    this.props.dispatch(openCreateCardModal({
      cardProps: {
        item: {
          sourceColumn: column,
        },
      },
    }));
  }

  openStockWtab(item) {
    this.props.dispatch(openStockWtabCardModal({ item }));
  }

  fetchBoardData() {
    this.props.dispatch(fetchBoardData());
  }

  addColumnIdToItems(c) {
    if (c.items === null) {
      return null;
    }
    return c.items.map(item => (
      {
        ...item,
        sourceColumn: c,
      }));
  }

  render() {
    let { columns } = this.props;
    const { cardModalOptions, wtabModal } = this.props;
    const isDesktop = window.innerWidth > 780;

    if (columns !== null) {
      columns = columns.map(c => ({
        ...c,
        items: this.addColumnIdToItems(c),
      }));
    }

    return (
      <div>
        <Helmet>
          <title>JUSTVULC | Dashboard</title>
        </Helmet>
        <ErrorBoundary>
          <UnscrollableColumnsLayout
            reduceHeightOffset={isDesktop ? '45px' : '0px'}
          >
            {
              (columns !== null && columns.length > 0) ?
                columns.map(column => (
                  <LayoutColumn
                    key={column.id}
                    column={column}
                    width={100 / columns.length}
                    name={column.name}
                    items={column.items}
                    onAddCardBtnClick={this.onAddCard}
                    onStockWtabDoubleClick={this.openStockWtab}
                  />
                ))
              :
                <div style={{
                    display: 'flex',
                    alignItems: 'center',
                    width: '100%',
                    height: '100%',
                    justifyContent: 'center',
                  }}
                >
                  <CircularProgress size={120} thickness={10} />
                </div>
            }
          </UnscrollableColumnsLayout>
          <CustomDragLayer width={100 / columns.length} />
          {
            cardModalOptions.open && (
              <EditCardModal
                options={cardModalOptions}
              />
            )
          }
          {
            wtabModal.open && (
              <WtabCardModal
                options={wtabModal}
              />
            )
          }
        </ErrorBoundary>
      </div>
    );
  }
}

Dashboard.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    id: PropTypes.number,
  })).isRequired,
  dispatch: PropTypes.func.isRequired,
  cardModalOptions: PropTypes.object.isRequired,
  wtabModal: PropTypes.object.isRequired,
  needRefreshBoardData: PropTypes.bool,
};

Dashboard.defaultProps = {
  needRefreshBoardData: false,
};
const mapStateToProps = ({ dashboard }) => (
  {
    columns: dashboard.columns,
    cardModalOptions: dashboard.cardModal,
    wtabModal: dashboard.wtabModal,
    needRefreshBoardData: dashboard.needRefreshBoardData,
  }
);

export default connect(
  mapStateToProps,
  null,
)(DragDropContext(HTML5Backend)(Dashboard));
