import { call, put, takeEvery, all } from 'redux-saga/effects';
import Api from '../../dashboard/services/api';

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* fetchBoard(action) {
  try {
    const res = yield call(Api.fetchBoard, action.payload);
    yield put({ type: 'BOARD_DATA_FETCH_SUCCEEDED', payload: res.data });
  } catch (e) {
    yield put({ type: 'BOARD_DATA_FETCH_FAILED', error: e });
  }
}

// worker Saga: will be fired on ENTRY_DATA_FETCH_REQUESTED actions
function* fetchEntry(action) {
  try {
    const res = yield call(Api.fetchEntry, action.payload);
    yield put({ type: 'ENTRY_DATA_FETCH_SUCCEEDED', payload: res.data });
  } catch (e) {
    yield put({ type: 'ENTRY_DATA_FETCH_FAILED', error: e });
  }
}

// worker Saga: will be fired on ENTRIES_DATA_FETCH_REQUESTED actions
function* fetchMovedCardsEntryData(action) {
  try {
    const requests = action.payload.checkedCards.map(id =>
      call(Api.fetchEntry, {
        item: {
          id,
        },
        entryType: action.payload.entryType,
      }));
    const res = yield all(requests);
    yield put({ type: 'ENTRIES_DATA_FETCH_SUCCEEDED', payload: res.map(({ data }) => data) });
  } catch (e) {
    yield put({ type: 'ENTRIES_DATA_FETCH_FAILED', error: e });
  }
}

// worker Saga: will be fired on MOVE_ENTRY_DATA_FETCH_REQUESTED actions
function* fetchMoveEntry(action) {
  try {
    const res = yield call(Api.fetchEntry, action.payload);
    yield put({ type: 'MOVE_ENTRY_DATA_FETCH_SUCCEEDED', payload: res.data });
  } catch (e) {
    yield put({ type: 'MOVE_ENTRY_DATA_FETCH_FAILED', error: e });
  }
}

// worker Saga: will be fired on ENTRY_DATA_FETCH_REQUESTED actions
function* fetchMaterialsAndServices(action) {
  try {
    const res = yield call(Api.fetchMaterialsAndServices, action.payload);
    yield put({ type: 'ITEMS_BY_ART_NUMBER_FETCH_SUCCEEDED', payload: res.data });
  } catch (e) {
    yield put({ type: 'ITEMS_BY_ART_NUMBER_FETCH_FAILED', error: e });
  }
}

// worker Saga: will be fired on SAVE_EDITED_CARD_REQUESTED actions
function* saveEditedCard(action) {
  try {
    const res = yield call(Api.saveEditedCard, action.payload);
    yield put({ type: 'SAVE_EDITED_CARD_SUCCEEDED', payload: res.data });
  } catch (e) {
    yield put({ type: 'SAVE_EDITED_CARD_FAILED', error: e });
  }
}

// worker Saga: will be fired on SAVE_MOVED_CARD_REQUESTED actions
function* saveMovedCard(action) {
  try {
    const res = yield call(Api.saveEditedCard, action.payload);
    yield put({ type: 'SAVE_MOVED_CARD_SUCCEEDED', payload: res.data });
  } catch (e) {
    yield put({ type: 'SAVE_MOVED_CARD_FAILED', error: e });
  }
}

// worker Saga: will be fired on CREATE_CARD_REQUESTED actions
function* createCard(action) {
  try {
    const res = yield call(Api.saveEditedCard, action.payload);
    yield put({ type: 'CREATE_CARD_SUCCEEDED', payload: res.data });
  } catch (e) {
    yield put({ type: 'CREATE_CARD_FAILED', error: e });
  }
}

// worker Saga: will be fired on CUSTOMERS_SEARCH_FETCH_REQUESTED actions
function* searchCustomer(action) {
  try {
    const res = yield call(Api.searchCustomer, action.payload);
    yield put({ type: 'CUSTOMERS_SEARCH_FETCH_SUCCEEDED', payload: res.data });
  } catch (e) {
    yield put({ type: 'CUSTOMERS_SEARCH_FETCH_FAILED', error: e });
  }
}

// worker Saga: will be fired on WTAB_DATA_FETCH_REQUESTED actions
function* fetchWtabData(action) {
  try {
    const res = yield call(Api.fetchWtab, action.payload);
    yield put({ type: 'WTAB_DATA_FETCH_SUCCEEDED', payload: res.data });
  } catch (e) {
    yield put({ type: 'WTAB_DATA_FETCH_FAILED', error: e });
  }
}

// worker Saga: will be fired on STOCK_WTAB_DATA_FETCH_REQUESTED actions
function* fetchStockWtabData(action) {
  try {
    const res = yield call(Api.fetchStockWtab, action.payload);
    yield put({ type: 'STOCK_WTAB_DATA_FETCH_SUCCEEDED', payload: res.data });
  } catch (e) {
    yield put({ type: 'STOCK_WTAB_DATA_FETCH_FAILED', error: e });
  }
}

/*
 * Starts fetchBoard on each dispatched `BOARD_DATA_FETCH_REQUESTED` action.
 */
function* dashboard() {
  yield takeEvery('BOARD_DATA_FETCH_REQUESTED', fetchBoard);
  yield takeEvery('WTAB_DATA_FETCH_REQUESTED', fetchWtabData);
  yield takeEvery('STOCK_WTAB_DATA_FETCH_REQUESTED', fetchStockWtabData);

  yield takeEvery('SAVE_EDITED_CARD_REQUESTED', saveEditedCard);
  yield takeEvery('CREATE_CARD_REQUESTED', createCard);
  yield takeEvery('SAVE_MOVED_CARD_REQUESTED', saveMovedCard);

  yield takeEvery('ENTRIES_DATA_FETCH_REQUESTED', fetchMovedCardsEntryData);
  yield takeEvery('ENTRY_DATA_FETCH_REQUESTED', fetchEntry);
  yield takeEvery('MOVE_ENTRY_DATA_FETCH_REQUESTED', fetchMoveEntry);
  yield takeEvery('CUSTOMERS_SEARCH_FETCH_REQUESTED', searchCustomer);
  yield takeEvery('ITEMS_BY_ART_NUMBER_FETCH_REQUESTED', fetchMaterialsAndServices);
}

export default dashboard;
