import styled from 'styled-components';

export const styles = {
  customContentStyle: {
    width: '100%',
    maxWidth: '99%',
  },
  formInput: {
    fontSize: '12px',
    width: '150px',
    height: '30px',
    lineHeight: '30px',
  },
  titleStyle: {
    fontSize: '16px',
    backgroundColor: 'rgb(0, 188, 212)',
    color: '#fff',
    padding: '5px 30px',
    display: 'block',
    marginBottom: '20px',
    height: '42px',
    boxSizing: 'border-box',
  },
};

export const CloseIcon = styled.span`
  font-size: 30px;
  float: right;
  cursor: pointer
`;

export const FormWrapper = styled.div`
  font-size: 12px;
`;

export const EmptyWtabMessage = styled.div`
  text-align: center;
  margin-top: 20px;
  font-size: 18px;
`;

export const ProgressWrapper = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  height: 100%;
  justify-content: center;
`;

export const TableCellText = styled.span`
  font-weight: ${props => props.fw};
`;
