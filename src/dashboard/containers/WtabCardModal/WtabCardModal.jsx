import React from 'react';
import { connect } from 'react-redux';
import Dialog from 'material-ui/Dialog';
import { TableHeaderColumn } from 'react-bootstrap-table';
import PropTypes from 'prop-types';
import { DateTime } from 'luxon';
import FlatButton from 'material-ui/FlatButton';
import CircularProgress from 'material-ui/CircularProgress';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css'; // eslint-disable-line

import { Table, CloseIcon } from '../../../shared';
import {
  closeWtabCardModal,
  fetchWtab,
  fetchStockWtab,
} from '../../actions';
import {
  styles,
  FormWrapper,
  EmptyWtabMessage,
  ProgressWrapper,
  TableCellText,
} from './styles';

/**
 * The dialog width has been set to occupy the full width
 * of browser through the `contentStyle` property.
 */
class Modal extends React.Component {
  constructor(props) {
    super(props);

    this.handleClose = this.handleClose.bind(this);
  }

  componentWillMount() {
    const {
      open,
      loading,
      item,
      stock,
    } = this.props.options;
    if (open && loading) {
      this.props.dispatch(stock ? fetchStockWtab({ item }) : fetchWtab({ item }));
    }
  }

  onRowClick(...args) {
    console.log(args);
  }

  getFooterRowFormatter(columnName) {
    return (tableData) => {
      const result =
        tableData
          .map(d => d[columnName].value)
          .reduce((curr, next) => curr + next);
      return (result === 0) ?
        null :
        <strong>&euro;{ result }</strong>;
    };
  }

  getFooter() {
    const columnsWithSummary = [
      'SENT',
      'AB',
      'PACK',
      'WIN',
      'Z',
      'WOUT',
      'MOVED',
      'DOC',
      'LIEF',
      'RE',
    ];

    return [
      [
        {
          label: 'Total',
          columnIndex: 0,
        },
        {
          label: '',
          columnIndex: 1,
        },
        {
          label: '',
          columnIndex: 2,
        },
        ...columnsWithSummary
          .map((col, i) => ({
            label: 'Total value',
            columnIndex: i + 3,
            align: 'center',
            formatter: this.getFooterRowFormatter(col),
          })),
      ],
    ];
  }

  getCloseDialogButton() {
    return <CloseIcon onClick={this.handleClose}>&#xd7;</CloseIcon>;
  }

  getModalBody({ data }) {
    if (data.length) {
      return (
        <FormWrapper onDrag={e => e.stopPropagation()}>
          <Table
            data={data}
            footerData={this.getFooter()}
            footer
            options={{
              expandRowBgColor: 'rgb(0, 188, 212)',
              onRowClick: this.onRowClick,
              expandBy: 'column',
            }}
            striped
            keyField="artNumber"
          >
            <TableHeaderColumn
              dataField="artNumber"
              headerAlign="center"
              dataAlign="center"
              width="100px"
            >
              Art. No.
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="description"
              width="300px"
              headerAlign="center"
            >
              Description
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="unit"
              headerAlign="center"
            >
              UNIT
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="SENT"
              dataFormat={this.getTableCellFormatter()}
              headerAlign="center"
              dataAlign="center"
            >
              SENT
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="AB"
              dataFormat={this.getTableCellFormatter()}
              headerAlign="center"
              dataAlign="center"
            >
              AB
            </TableHeaderColumn>
            <TableHeaderColumn
              dataFormat={this.getTableCellFormatter()}
              dataField="PACK"
              headerAlign="center"
              dataAlign="center"
            >
              PACK
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="WIN"
              headerAlign="center"
              dataFormat={this.getTableCellFormatter()}
              dataAlign="center"
            >
              WIN
            </TableHeaderColumn>
            <TableHeaderColumn
              dataFormat={this.getTableCellFormatter(true)}
              dataField="Z"
              headerAlign="center"
              dataAlign="center"
            >
              Z
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="WOUT"
              dataFormat={this.getTableCellFormatter()}
              headerAlign="center"
              dataAlign="center"
            >
              WOUT
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="MOVED"
              headerAlign="center"
              dataFormat={this.getTableCellFormatter()}
              dataAlign="center"
            >
              MOVED
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="DOC"
              dataFormat={this.getTableCellFormatter()}
              headerAlign="center"
              dataAlign="center"
              expandable={false}
            >
              DOC
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="LIEF"
              headerAlign="center"
              dataFormat={this.getTableCellFormatter()}
              dataAlign="center"
              expandable={false}
            >
              LIEF
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="RE"
              headerAlign="center"
              dataFormat={this.getTableCellFormatter()}
              dataAlign="center"
              expandable={false}
            >
              RE
            </TableHeaderColumn>
          </Table>
        </FormWrapper>
      );
    }
    return <EmptyWtabMessage>No data</EmptyWtabMessage>;
  }


  getTableCellFormatter(isZ) {
    return ({ value, contrAgent }) => {
      const v = value === 0 ? '' : value;
      return (
        <React.Fragment>
          <TableCellText fw={isZ ? 'bold' : 'normal'}>{ v }</TableCellText> <br />
          <a href="javasctipt:void(0)">{ contrAgent }</a>
        </React.Fragment>
      );
    };
  }

  handleClose() {
    this.props.dispatch(closeWtabCardModal());
  }

  render() {
    const {
      open,
      item,
      loading,
      stock,
    } = this.props.options;

    const title = stock ? `STOCK, ${DateTime.fromSQL(item.date).toFormat('dd.MM.yy')}` :
      `${item.name}, ${DateTime.fromSQL(item.date).toFormat('dd.MM.yy')}, #${item.id}`;

    const actions = loading ? [] : [
      <FlatButton
        label="Cancel"
        primary
        onClick={this.handleClose}
      />,
    ];

    return (
      <Dialog
        title={
          <span>
            { title }{this.getCloseDialogButton()}
          </span>}
        titleStyle={styles.titleStyle}
        actions={actions}
        modal
        autoScrollBodyContent
        contentStyle={styles.customContentStyle}
        open={open}
      >
        {
          loading && (
            <ProgressWrapper>
              <CircularProgress size={120} thickness={10} />
            </ProgressWrapper>
          )
        }
        {
          !loading && this.getModalBody(this.props.options)
        }
      </Dialog>
    );
  }
}

Modal.propTypes = {
  options: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default connect()(Modal);
