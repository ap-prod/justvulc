import React from 'react';
import { PropTypes } from 'prop-types';
import { DragSource } from 'react-dnd';
import { getEmptyImage } from 'react-dnd-html5-backend';
import { connect } from 'react-redux';
import { ColumnCard } from '../../components';
import {
  moveCard,
  removeCard,
  editCard,
} from '../../actions';

const cardSource = {
  beginDrag(props) {
    return { item: props.item };
  },
  endDrag(props, monitor) {
    const results = monitor.getDropResult();
    if (!results) return null;
    const { move, remove, column } = results;
    if (move) {
      props.dispatch(moveCard({
        item: props.item,
        targetColumn: column,
      }));
    }

    if (remove) {
      props.dispatch(removeCard({
        item: props.item,
        targetColumn: column,
      }));
    }
    return { item: props.item };
  },
};

function collect(conn) {
  return {
    connectDragSource: conn.dragSource(),
    connectDragPreview: conn.dragPreview(),
  };
}

class ColumnDraggableCard extends React.Component {
  constructor(props) {
    super(props);

    this.onEditCardBtnClick = this.onEditCardBtnClick.bind(this);
  }
  componentDidMount() {
    this.props.connectDragPreview(getEmptyImage(), {
      captureDraggingState: true,
    });
  }

  onEditCardBtnClick() {
    this.props.dispatch(editCard({
      cardProps: this.props,
    }));
  }

  render() {
    return this.props.connectDragSource((
      <div>
        <ColumnCard {...this.props} onEditCardBtnClick={this.onEditCardBtnClick} />
      </div>
    ));
  }
}

ColumnDraggableCard.propTypes = {
  connectDragPreview: PropTypes.func.isRequired,
  connectDragSource: PropTypes.func.isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default connect()(DragSource('card', cardSource, collect)(ColumnDraggableCard));
