import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ColumnDraggableCard } from '../';
import { checkCard, openWtabCardModal } from '../../actions';

class ColumnCardList extends React.Component {
  constructor(props) {
    super(props);
    this.onCardCheck = this.onCardCheck.bind(this);
    this.onDoubleClick = this.onDoubleClick.bind(this);
  }

  onCardCheck({ name, id }, value) {
    const { column } = this.props;
    let checkedContsAgent = name;
    let checkedCards = [...column.checkedCards];
    if (!value) {
      checkedCards = column.checkedCards.filter(cardId => cardId !== id);
      checkedContsAgent = checkedCards.length === 0 ? '' : checkedContsAgent;
    } else {
      checkedCards.push(id);
    }
    this.props.dispatch(checkCard({
      checkData: {
        checkedContsAgent,
        checkedCards,
      },
      columnId: column.id,
    }));
  }

  onDoubleClick(item) {
    this.props.dispatch(openWtabCardModal({ item }));
  }

  render() {
    const { items } = this.props;
    const { checkedCards, checkedContsAgent } = this.props.column;
    const cards = items
      .map(item => (
        <ColumnDraggableCard
          isChecked={checkedCards.filter(c => c === item.id).length > 0}
          disabledCheckbox={checkedCards.length !== 0 && checkedContsAgent !== item.name}
          key={item.id}
          item={item}
          onCardCheck={this.onCardCheck}
          onDoubleClick={this.onDoubleClick}
        />
      ));
    return cards;
  }
}
ColumnCardList.propTypes = {
  dispatch: PropTypes.func.isRequired,
  column: PropTypes.object.isRequired,
  items: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    processingDuration: PropTypes.string,
    amount: PropTypes.number,
    id: PropTypes.number,
  })),
};

ColumnCardList.defaultProps = {
  items: [],
};

export default connect()(ColumnCardList);
