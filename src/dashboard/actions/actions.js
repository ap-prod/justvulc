import { createAction } from '../../utils';

export const addCard = createAction('ADD_CARD');
export const moveCard = createAction('MOVE_CARDS');
export const removeCard = createAction('REMOVE_CARD');
export const checkCard = createAction('CHECK_CARD');
export const editCard = createAction('OPEN_EDIT_CARD_MODAL');

export const fetchBoardData = createAction('BOARD_DATA_FETCH_REQUESTED');
export const fetchUserData = createAction('USER_DATA_FETCH_REQUESTED');
export const fetchWtab = createAction('WTAB_DATA_FETCH_REQUESTED');
export const fetchStockWtab = createAction('STOCK_WTAB_DATA_FETCH_REQUESTED');

export const openCreateCardModal = createAction('OPEN_CREATE_CARD_MODAL');
export const openWtabCardModal = createAction('OPEN_WTAB_CARD_MODAL');
export const closeWtabCardModal = createAction('CLOSE_WTAB_CARD_MODAL');
