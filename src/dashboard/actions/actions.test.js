import {
  addCard,
  moveCard,
  removeCard,
  fetchUserData,
  fetchBoardData,
  checkCard,
} from './';

describe('Dashboard action creators', () => {
  it('should return add card action', () => {
    const cardMock = { name: 'test' };
    const action = addCard(cardMock);
    expect(action.type).toBe('ADD_CARD');
    expect(action.payload).toBe(cardMock);
  });

  it('should return move card action', () => {
    const cardMock = { name: 'test' };
    const action = moveCard(cardMock);
    expect(action.type).toBe('MOVE_CARDS');
    expect(action.payload).toBe(cardMock);
  });

  it('should return remove card action', () => {
    const cardMock = { name: 'test' };
    const action = removeCard(cardMock);
    expect(action.type).toBe('REMOVE_CARD');
    expect(action.payload).toBe(cardMock);
  });

  it('should return check card action', () => {
    const cardMock = {
      columnId: 1231,
      checkData: {
        checkedContsAgent: '',
        checkedCards: [],
      },
    };
    const action = checkCard(cardMock);
    expect(action.type).toBe('CHECK_CARD');
    expect(action.payload).toBe(cardMock);
  });

  it('should return fetch board action', () => {
    const payloadMock = [{ name: 'test' }];
    const action = fetchBoardData(payloadMock);
    expect(action.type).toBe('BOARD_DATA_FETCH_REQUESTED');
    expect(action.payload).toBe(payloadMock);
  });

  it('should return fetch user action', () => {
    const payloadMock = [{ cookie: 'test' }];
    const action = fetchUserData(payloadMock);
    expect(action.type).toBe('USER_DATA_FETCH_REQUESTED');
    expect(action.payload).toBe(payloadMock);
  });
});
