import styled from 'styled-components';

export const FormWrapper = styled.div`
  font-size: 12px;
`;

export const CloseIcon = styled.span`
  font-size: 30px;
  float: right;
  cursor: pointer
`;

export const styles = {
  customContentStyle: {
    width: '100%',
    maxWidth: '99%',
  },
  titleStyle: {
    fontSize: '16px',
    backgroundColor: 'rgb(0, 188, 212)',
    color: '#fff',
    padding: '5px 30px',
    display: 'block',
    marginBottom: '20px',
  },
};
