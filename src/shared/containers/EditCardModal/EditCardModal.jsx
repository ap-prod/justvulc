import React from 'react';
import { connect } from 'react-redux';
import Dialog from 'material-ui/Dialog';
import PropTypes from 'prop-types';
import FlatButton from 'material-ui/FlatButton';
import CircularProgress from 'material-ui/CircularProgress';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css'; // eslint-disable-line
import debounce from 'lodash/debounce';

import {
  closeEditModal,
  fetchItemsByArtNumber,
  fetchEntry,
  fetchMoveEntryData,
  fetchMovedCardsEntryData,
  changeCustomerData,
  addMaterialsAndServicesRow,
  editMaterialsAndServicesRow,
  removeMaterialsAndServicesRow,
  saveEditedCard,
  createCard,
  saveMovedCard,
  searchCustomer,
  clearSearchCustomer,
} from '../../actions';
import {
  moveCard,
} from '../../../dashboard';
import { styles, FormWrapper } from './styles';
import { CloseIcon } from '../../../shared';
import { MainTable, CustomerForm, SummaryTable } from '../../components';

/**
 * The dialog width has been set to occupy the full width
 * of browser through the `contentStyle` property.
 */
class Modal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      expanding: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.onRemoveRow = this.onRemoveRow.bind(this);
    this.onExpandRow = this.onExpandRow.bind(this);
    this.onAddRow = this.onAddRow.bind(this);
    this.onEditRow = this.onEditRow.bind(this);
    this.onSearchArtNumder = this.onSearchArtNumder.bind(this);
    this.handleCustomerFormChange = this.handleCustomerFormChange.bind(this);
    this.onSearchCustomer = this.onSearchCustomer.bind(this);
  }

  componentWillMount() {
    const {
      open,
      loading,
      move,
      create,
      update,
      item,
      targetColumn,
    } = this.props.options;

    if (open && (update || create) && loading) {
      !item.id && (item.id = 0); // eslint-disable-line
      this.props.dispatch(fetchEntry({
        item,
        entryType: item.sourceColumn.id,
      }));
    }

    if (open && move && loading) {
      const { checkedCards } = this.props.options.item.sourceColumn;
      /* eslint-disable */
      !checkedCards.length ?
        this.props.dispatch(fetchMoveEntryData({
          item: this.props.options.item,
          entryType: targetColumn.id,
        })) :
        this.props.dispatch(fetchMovedCardsEntryData({
          checkedCards,
          entryType: targetColumn.id,
        }));
      /* eslint-enable */
    }
  }

  onRemoveRow(row) {
    this.props.dispatch(removeMaterialsAndServicesRow({ row }));
  }

  onExpandRow(row) {
    let expanding;
    if (this.state.expanding.includes(row.artNumber)) {
      expanding = this.state.expanding.filter(index => index !== row.artNumber);
    } else {
      expanding = [...this.state.expanding, row.artNumber];
    }
    this.setState({
      expanding,
    });
  }

  onAddRow(row) {
    this.props.dispatch(addMaterialsAndServicesRow({ row }));
  }

  onEditRow(row, rowIndex) {
    this.props.dispatch(editMaterialsAndServicesRow({ row, rowIndex }));
  }

  onSearchArtNumder(value) {
    this.props.dispatch(fetchItemsByArtNumber({
      value,
    }));
  }

  onSearchCustomer(queryStr) {
    if (queryStr) {
      this.props.dispatch(searchCustomer(queryStr));
    } else {
      this.props.dispatch(clearSearchCustomer());
    }
  }

  getCloseDialogButton() {
    return <CloseIcon onClick={this.handleClose}>&#xd7;</CloseIcon>;
  }

  getModalBody({
    data,
    seachedMaterials,
    create,
    searchedCustomers,
  }) {
    if (data) {
      const {
        totalErrorsCount,
        mAS,
      } = this.validateMaterialsAndServices(data.materialsAndServices);
      return (
        <FormWrapper onDrag={e => e.stopPropagation()}>
          <CustomerForm
            data={data.customerData}
            onChange={this.handleCustomerFormChange}
            deliveryTermsOptions={data.deliveryTermsOptions}
            deliveryAddressOptions={data.deliveryAddressOptions}
            paymentTermOptions={data.paymentTermsOptions}
            vatOptions={data.vatOptions}
            isCreateForm={create}
            searchedCustomers={searchedCustomers}
            onSearchCustomer={this.onSearchCustomer}
          />
          <MainTable
            data={mAS}
            expanding={this.state.expanding}
            onRemoveRow={this.onRemoveRow}
            onExpandRow={this.onExpandRow}
            onEditRow={this.onEditRow}
            onAddRow={this.onAddRow}
            searchedItems={seachedMaterials}
            onSearchArtNumder={debounce(this.onSearchArtNumder, 400)}
          />
          <SummaryTable
            data={mAS}
            vat={data.customerData.VAT}
            totalErrorsCount={totalErrorsCount}
          />
        </FormWrapper>
      );
    }
    return null;
  }

  validateMaterialsAndServices(mAS) {
    let totalErrorsCount = 0; // eslint-disable-line
    return {
      mAS: mAS.map((d) => {
        const {
          description,
          price,
          pm,
          qty,
          pu,
          netW,
          pp,
          isService,
        } = d;
        let errors = [];
        if (
          description.length === 0 ||
          description.filter(({ value }) => !value || value.length === 0).length
        ) {
          errors.push('description');
        }
        if (
          description.length === 0 ||
          description.filter(({ measure }) => !measure || measure.length === 0).length
        ) {
          errors.push('measure');
        }
        if (
          price === 0 ||
          (['Z', 'WOUT', 'MOVED', 'DOC', 'LIEF'].includes(this.props.options.item.sourceColumn.name) && price <= pm) ||
          (['SENT', 'AB', 'PACK'].includes(this.props.options.item.sourceColumn.name) && price > pp)
        ) {
          errors.push('price');
        }
        if (parseFloat(qty) === 0) {
          errors.push('qty');
        }
        if (pu === 0) {
          errors.push('pu');
        }
        if (parseFloat(netW) === 0) {
          errors.push('netW');
        }
        if (isService) {
          errors = [];
        }
        totalErrorsCount += errors.length;
        return { ...d, errors };
      }),
      totalErrorsCount,
    };
  }

  handleCustomerFormChange(data) {
    this.props.dispatch(changeCustomerData({
      customerData: data,
    }));
  }

  handleClose() {
    // eslint-disable-next-line
    this.props.move && this.props.dispatch(moveCard({
      item: this.props.options.item,
      targetColumn: this.props.options.item.sourceColumn,
    }));
    this.props.dispatch(closeEditModal({
      cancel: true,
    }));
  }

  handleSubmit() {
    const {
      customerData,
      materialsAndServices,
      entries,
    } = this.props.options.data;
    const {
      create,
      update,
      move,
      item,
      targetColumn,
    } = this.props.options;

    if (update) {
      this.props.dispatch(saveEditedCard({
        customerData,
        materialsAndServices,
      }));
    }

    if (create || entries.length) {
      this.props.dispatch(createCard({
        customerData: {
          ...customerData,
          eventTypeCode: item.sourceColumn.id,
          entryCode: 0,
        },
        materialsAndServices,
      }));
    }

    if (move && !entries.length) {
      this.props.dispatch(saveMovedCard({
        customerData: {
          ...customerData,
          eventTypeCode: targetColumn.id,
          entryCode: item.id,
        },
        materialsAndServices,
      }));
    }
  }

  render() {
    const {
      open,
      title,
      loading,
      create,
      update,
      move,
      data,
    } = this.props.options;

    let submitBtnLabel = '';

    if (update) {
      submitBtnLabel = 'Update';
    }

    if (create) {
      submitBtnLabel = 'Add';
    }

    if (move) {
      submitBtnLabel = 'Entry';
    }

    const actions = loading ? [] : [
      <FlatButton
        label={submitBtnLabel}
        primary
        disabled={create && !data.customerData.customerId}
        onClick={this.handleSubmit}
      />,
      <FlatButton
        label="Cancel"
        primary
        onClick={this.handleClose}
      />,
    ];

    return (
      <Dialog
        title={
          <span>
            {title}{this.getCloseDialogButton()}
          </span>}
        titleStyle={styles.titleStyle}
        actions={actions}
        modal
        autoScrollBodyContent
        contentStyle={styles.customContentStyle}
        open={open}
      >
        {
          loading && (
            <div style={{
              display: 'flex',
              alignItems: 'center',
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              }}
            >
              <CircularProgress size={120} thickness={10} />
            </div>
          )
        }
        {
          !loading && this.getModalBody(this.props.options)
        }
      </Dialog>
    );
  }
}

Modal.propTypes = {
  options: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default connect()(Modal);
