import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

export const StyledNavLink = styled(NavLink)`
  color: #fff;
  font-size: 16px;
  margin: 0 15px;
  text-decoration: none;
  transition: .3s;
  display: inline-flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  &:after {
    display: block;
    content: '';
    height: 2px;
    width: 0;
    transition: .6s;
    margin: -6px -15px 0 -15px;
    background-color: #fff;
  }
  &.active {
    font-weight: bold;
    &:after {
      width: calc(100% + 30px);
    }
  }
`;

export const HeaderSearchInputIcon = styled.span`
  color: #fff;
  position: absolute;
  margin-top: 14px;
  margin-left: 53px;
  margin-right: -30px;
  width: 11px;
  height: 11px;
  border: solid 2px currentColor;
  border-radius: 100%;
  -webkit-transform: rotate(-45deg);
          transform: rotate(-45deg);
  &:before{
    content: '';
    position: absolute;
    top: 12px;
    left: 5px;
    height: 8px;
    width: 2px;
    background-color: currentColor;
  }
`;

export const HeaderSearchInput = styled.input`
  font: inherit;
  font-size: 16px;
  width: 200px;
  height: 30px;
  box-sizing: border-box;
  border: 0;
  margin: -5px 0 0 40px;
  padding: 8px 8px 8px 52px;
  display: inline-block;
  background: none;
  white-space: normal;
  vertical-align: middle;
  color: #fff;
  background-color: #80DEEA;
  transition: width .3s;
  outline: none;
  &:focus {
    width: 300px;
  }
  &:focus, &:hover {
    background-color: #4DD0E1;
  }
`;
