import React from 'react';
import PropTypes from 'prop-types';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getCookie, redirectToLogin } from '../../../utils';
import { fetchUserData } from '../../../dashboard';
import { logoutUser } from '../../actions';
import {
  StyledNavLink,
  HeaderSearchInputIcon,
  HeaderSearchInput,
} from './styles';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);
    this.getHeaderLeftBar = this.getHeaderLeftBar.bind(this);

    this.menu = [
      {
        label: 'Contacts',
        path: '/contacts',
      }, {
        label: 'Marketing',
        path: '/marketing',
      }, {
        label: 'Inquiries',
        path: '/inquiries',
      }, {
        label: 'Deals',
        path: '/deals',
      }, {
        label: 'Dashboard',
        path: '/dashboard',
      }, {
        label: 'Entries',
        path: '/entries',
      }, {
        label: 'Reports',
        path: '/reports',
      },
    ];
  }

  componentDidMount() {
    this.fetchUserData();
  }

  componentDidUpdate() {
    if (process.env.NODE_ENV === 'production' && this.props.unautoraized) {
      redirectToLogin();
    }
  }

  getHeaderLeftBar() {
    return (
      <React.Fragment>
        {
          this.menu.map(({ label, path }) => (
            <StyledNavLink
              exact
              key={path}
              to={path}
            >
              {label}
            </StyledNavLink>
          ))
        }
        <HeaderSearchInputIcon />
        <HeaderSearchInput />
        {this.isTestServer()}
      </React.Fragment>
    );
  }

  isTestServer() {
    if (window.DEV) {
      return (
        <div
          style={{
            position: 'fixed',
            bottom: '-15px',
            fontWeight: 'bold',
            left: '10px',
            color: 'red',
          }}
        >
          TEST SERVER
        </div>
      );
    }
    return null;
  }

  fetchUserData() {
    this.props.dispatch(fetchUserData({ cookie: getCookie(process.env.COOKIE_NAME) }));
  }

  logout() {
    this.props.dispatch(logoutUser({
      cookie: getCookie(process.env.COOKIE_NAME),
      login: this.props.user.login,
    }));
  }

  render() {
    const { user } = this.props;
    const isDesktop = window.innerWidth > 780;

    if (isDesktop) {
      return (
        <AppBar
          iconElementRight={
            <div style={{ marginTop: '-4px', color: '#fff', fontWeight: '600' }}>
              {user && user.name}
              <RaisedButton
                onClick={this.logout}
                label="Logout"
                style={{ marginLeft: '15px', cursor: 'pointer' }}
              />
            </div>
          }
          title={this.getHeaderLeftBar()}
          titleStyle={{ height: '45px', lineHeight: '45px', fontSize: '13px' }}
          showMenuIconButton={false}
        />
      );
    }

    return null;
  }
}

Header.propTypes = {
  user: PropTypes.shape({
    name: PropTypes.string,
    login: PropTypes.string,
    id: PropTypes.number,
  }),
  unautoraized: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired,
};

Header.defaultProps = {
  user: null,
};

const mapStateToProps = ({ dashboard }) => (
  {
    user: dashboard.user,
    unautoraized: dashboard.unautoraized,
  }
);

export default withRouter(connect(
  mapStateToProps,
  null,
)(Header));
