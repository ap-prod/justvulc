import React from 'react';
import PropTypes from 'prop-types';
import {
  SummaryTableWrapper,
  SummaryTableEl,
  SummaryTableTd,
} from './styles';

class SummaryTable extends React.Component {
  getTotalNettoWeight(items) {
    let result = 0;
    if (items.length) {
      items.forEach((it) => {
        result += it.netW * it.qty;
      });
    }
    return result.toFixed(3);
  }

  calcTotal(items) {
    let result = 0;
    if (items.length) {
      items.forEach((it) => {
        result += (it.price * it.qty) / it.pu;
      });
    }
    if (!Number.isFinite(result)) {
      return 0;
    }
    return result.toFixed(2);
  }

  render() {
    const { data, vat, totalErrorsCount } = this.props;
    if (!data.length) {
      return null;
    }
    const services = data.filter(({ isService }) => isService);
    const materials = data.filter(({ isService }) => !isService);

    const vatValue = (this.calcTotal(data) * (+vat)) / 100;
    const totalMaterials = this.calcTotal(materials);
    const totalServices = this.calcTotal(services);
    const weightNetto = this.getTotalNettoWeight(data);
    const weightBrutto = this.getTotalNettoWeight(data) * 1.03;

    return (
      <SummaryTableWrapper>
        <SummaryTableEl>
          <tbody>
            <tr>
              <SummaryTableTd>
                <b>Total Materials:</b>
              </SummaryTableTd>
              <SummaryTableTd>
                &euro; { totalMaterials }
              </SummaryTableTd>
              <SummaryTableTd>
                <b>Total services:</b>
              </SummaryTableTd>
              <SummaryTableTd>
                &euro; { totalServices }
              </SummaryTableTd>
              <SummaryTableTd>
                <b>Weight Netto:</b>
              </SummaryTableTd>
              <SummaryTableTd>
                { weightNetto } Kg
              </SummaryTableTd>
              <SummaryTableTd>
                <b>Weight Brutto:</b>
              </SummaryTableTd>
              <SummaryTableTd>
                { weightBrutto.toFixed(3) } Kg
              </SummaryTableTd>
            </tr>
            <tr>
              <SummaryTableTd>
                <b>Total without VAT:</b>
              </SummaryTableTd>
              <SummaryTableTd>
                &euro; { Number(+totalMaterials + +totalServices).toFixed(2) }
              </SummaryTableTd>
              <SummaryTableTd>
                <b>Total with VAT:</b>
              </SummaryTableTd>
              <SummaryTableTd>
                &euro; { Number(vatValue + +totalMaterials + +totalServices).toFixed(2) }
              </SummaryTableTd>
              <SummaryTableTd>
                <b>VAT:</b>
              </SummaryTableTd>
              <SummaryTableTd>
                &euro; { vatValue.toFixed(2) }
              </SummaryTableTd>
              <SummaryTableTd>
                <b>Total Errors:</b>
              </SummaryTableTd>
              <SummaryTableTd>
                { totalErrorsCount }
              </SummaryTableTd>
            </tr>
          </tbody>
        </SummaryTableEl>
      </SummaryTableWrapper>
    );
  }
}

SummaryTable.propTypes = {
  data: PropTypes.array.isRequired,
  vat: PropTypes.number,
  totalErrorsCount: PropTypes.number.isRequired,
};

SummaryTable.defaultProps = {
  vat: 0,
};

export default SummaryTable;
