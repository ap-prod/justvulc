import styled from 'styled-components';

export const SummaryTableWrapper = styled.div`
  margin-top: 20px;
`;

export const SummaryTableEl = styled.table`
  border-collapse: collapse;
`;

export const SummaryTableTd = styled.td`
  padding: 10px 30px 10px 0;
  border-bottom: 1px solid #00bcd4;
`;
