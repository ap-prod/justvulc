import React from 'react';
import PropTypes from 'prop-types';
import icons from './icons';

/**
 * Thrash
 */
class Icon extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hovered: false,
    };

    this.handleMouseEnter = this.handleMouseEnter.bind(this);
    this.handleMouseLeave = this.handleMouseLeave.bind(this);
  }

  handleMouseEnter() {
    this.setState({
      hovered: true,
    });
  }

  handleMouseLeave() {
    this.setState({
      hovered: false,
    });
  }

  render() {
    const {
      style,
      hoveredStyle,
      title,
      icon,
    } = this.props;
    let compiledStyle = style;
    if (this.state.hovered) {
      compiledStyle = { ...style, ...hoveredStyle };
    }
    return (
      <span
        className="trash"
        onMouseEnter={this.handleMouseEnter}
        onMouseLeave={this.handleMouseLeave}
        title={title}
      >
        <img
          src={icons[icon]}
          alt="amount"
          style={compiledStyle}
        />
      </span>
    );
  }
}

Icon.propTypes = {
  style: PropTypes.object.isRequired,
  hoveredStyle: PropTypes.object,
  title: PropTypes.string,
  icon: PropTypes.string.isRequired,
};

Icon.defaultProps = {
  hoveredStyle: {},
  title: '',
};

export default Icon;
