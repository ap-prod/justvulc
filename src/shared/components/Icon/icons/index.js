import bin from './bin.svg';
import close from './close.svg';
import summ from './the-sum-of-mathematical-symbol.svg';

export default {
  bin,
  summ,
  close,
};
