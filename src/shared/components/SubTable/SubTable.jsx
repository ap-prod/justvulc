
import React from 'react';
import { TableHeaderColumn } from 'react-bootstrap-table';
import PropTypes from 'prop-types';
import { Table } from '../../../shared';

const BSTable = ({ data, columns }) => (
  <Table data={[data]}>
    {
      columns.map((col, i) => (
        <TableHeaderColumn
          dataField={col.propName}
          isKey={i === 0}
          key={`subtable_${col.artNumber}`}
          headerAlign="center"
          dataAlign="center"
        >
          {
            col.title
          }
        </TableHeaderColumn>
      ))
    }
  </Table>
);

BSTable.propTypes = {
  data: PropTypes.object.isRequired,
  columns: PropTypes.array.isRequired,
};

export default BSTable;
