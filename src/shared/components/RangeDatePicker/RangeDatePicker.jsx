import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'material-ui/DatePicker';
import { DateTime } from 'luxon';
import { RangeDatePickerWrapper, styles } from './styles';

class RangeDatePicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      min: props.defaultValue.min,
      max: props.defaultValue.max,
    };
    this.cleanFiltered = this.cleanFiltered.bind(this);
  }

  cleanFiltered() {
    this.setState({
      min: null,
      max: null,
    });
  }

  dateFilter(field) {
    return (e, v) => {
      this.setState({
        [field]: v,
      });
      this.props.onChange(this.state);
    };
  }

  render() {
    return (
      <RangeDatePickerWrapper>
        <DatePicker
          textFieldStyle={styles.dataPickerTextField}
          name="min"
          maxDate={this.state.max}
          value={this.state.min}
          formatDate={d => DateTime.fromJSDate(d).toFormat('dd.MM.yyyy')}
          onChange={this.dateFilter('min')}
          container="inline"
          autoOk
        />
        --
        <DatePicker
          textFieldStyle={styles.dataPickerTextField}
          name="max"
          minDate={this.state.min}
          value={this.state.max}
          formatDate={d => DateTime.fromJSDate(d).toFormat('dd.MM.yyyy')}
          onChange={this.dateFilter('max')}
          container="inline"
          autoOk
        />
      </RangeDatePickerWrapper>
    );
  }
}

RangeDatePicker.propTypes = {
  onChange: PropTypes.func.isRequired,
  defaultValue: PropTypes.object,
};

RangeDatePicker.defaultProps = {
  defaultValue: {
    min: null,
    max: null,
  },
};

export default RangeDatePicker;
