import styled from 'styled-components';

export const RangeDatePickerWrapper = styled.div`
  font: inherit;
  font-style: normal;
  box-sizing: border-box;
  border: 0;
  display: inline-block;
  height: 32px;
  margin-top: 3px;
  background: none;
  white-space: normal;
  vertical-align: middle;
  color: #000;
  background-color: #E1F5FE;
  transition: .3s;
  display: flex;
  justify-content: space-between;
  align-items: center;
  outline: none;
  & input[name=min] {
    text-align: right;
  }
`;

export const styles = {
  dataPickerTextField: { width: '75px', fontSize: '13px' },
};
