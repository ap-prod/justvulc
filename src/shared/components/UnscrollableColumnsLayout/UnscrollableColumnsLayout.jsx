import React from 'react';
import PropTypes from 'prop-types';
import { UnscrollableLayout } from './styles';

/**
 * Verticaly unscrollable columns layout
 */
const UnscrollableColumnsLayout = ({ children, reduceHeightOffset }) =>
  <UnscrollableLayout reduceHeightOffset={reduceHeightOffset}>{children}</UnscrollableLayout>;

UnscrollableColumnsLayout.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.element), PropTypes.element]),
  reduceHeightOffset: PropTypes.string,
};

UnscrollableColumnsLayout.defaultProps = {
  reduceHeightOffset: '0px',
  children: [],
};

export default UnscrollableColumnsLayout;
