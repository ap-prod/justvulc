import styled from 'styled-components';

export const UnscrollableLayout = styled.div`
  display: flex;
  width: 100%;
  padding: 0 0px 10px 5px;
  margin: 0 auto;
  justify-content: flex-start;
  flex-direction: row;
  box-sizing: border-box;
  overflow-x: auto;
  overflow-y: hidden;
  height: ${props => `calc(100vh - ${props.reduceHeightOffset})`}
`;
