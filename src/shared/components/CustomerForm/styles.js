import styled from 'styled-components';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import SelectField from 'material-ui/SelectField';

export const FormGroup = styled.div`
  margin-bottom: 20px;
  margin-right: 20px;
  ${({ w }) => w && `width: ${w || '100%'}`};
`;

export const FormLabel = styled.span`
  padding-right: 10px;
  width: ${props => props.w || 'auto'};
`;

export const FormRow = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
`;

export const FormControl = styled.div`
  display: flex;
  align-items: center;
  padding-right: 10px;
  justify-content: flex-start;
`;

export const FormInputWrap = styled(TextField)`
  height: 36px !important;
  width: 100% !important;
`;

export const FormDataPicker = styled(DatePicker)`
  height: 36px !important;
  width: 100% !important;
`;

export const FormSelect = styled(SelectField)`
  font-size: 12px !important;
  width: 100% !important;
  height: 30px !important;
`;

export const styles = {
  menuStyle: {
    borderRadius: '1px',
    border: '1px solid #00bcd4',
    background: '#fff',
    fontSize: '12px',
    position: 'fixed',
    display: 'block',
    width: 'auto%',
    maxWidth: '900px',
    left: 'auto',
    top: 'auto',
    zIndex: 1,
  },
  formInput: {
    fontSize: '12px',
    width: '100%',
    height: '30px',
    lineHeight: '30px',
  },
  selectButton: {
    fontSize: '12px',
    width: '30px',
    padding: '0',
    height: '30px',
    lineHeight: '30px',
  },
};
