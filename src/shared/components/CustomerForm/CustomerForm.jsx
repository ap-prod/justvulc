
import React from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/MenuItem';
import { DateTime } from 'luxon';
import Autocomplete from 'react-autocomplete';
import {
  FormRow,
  FormGroup,
  FormControl,
  FormLabel,
  FormInputWrap,
  FormDataPicker,
  FormSelect,
  styles,
} from './styles';

class CustomerForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      customerControlValue: '',
      autocompleteOpen: false,
    };
  }

  getCustomerAutocomplete() {
    return (
      <Autocomplete
        openOnFocus
        open={this.state.autocompleteOpen}
        items={this.props.searchedCustomers}
        getItemValue={item => item.name}
        menuStyle={styles.menuStyle}
        theme={styles.formInput}
        alwaysRenderSuggestions
        renderItem={(item, highlighted) => (
          <div
            key={item.id}
            style={{
              backgroundColor: highlighted ? '#ddd' : '#fff',
              padding: '3px 15px',
              cursor: 'pointer',
            }}
          >
            {item.name} -- ({item.id})
          </div>
        )}
        value={this.state.customerControlValue}
        onChange={(e) => {
          this.props.onSearchCustomer(e.target.value);
          this.setState({
            autocompleteOpen: true,
            customerControlValue: e.target.value,
          });
        }}
        onSelect={(value, item) => {
          this.props.onChange({
            ...this.props.data,
            customerCode: item.id,
            customerId: item.name,
          });
          this.setState({
            autocompleteOpen: false,
            customerControlValue: value,
          });
        }}
      />
    );
  }

  handleFormValueChange(controlName) {
    return (e, date, payload) => {
      this.props.onChange({
        ...this.props.data,
        [controlName]: !e ? DateTime.fromJSDate(date).toSQLDate() : e.target.value ? e.target.value : payload, //eslint-disable-line
      });
    };
  }

  render() {
    const {
      data,
      deliveryTermsOptions,
      deliveryAddressOptions,
      paymentTermOptions,
      vatOptions,
      isCreateForm,
    } = this.props;

    return (
      <FormRow>
        <FormGroup w="30%">
          <FormControl>
            <FormLabel w="110px">{ isCreateForm ? 'Customer' : 'Customer ID' }:</FormLabel>
            { isCreateForm ?
              this.getCustomerAutocomplete() :
              <FormInputWrap
                value={data.customerId}
                inputStyle={styles.formInput}
                onChange={this.handleFormValueChange('customerId')}
                name="customerId"
              />
            }
          </FormControl>
          <FormControl>
            <FormLabel w="110px">Document No.:</FormLabel>
            <FormInputWrap
              inputStyle={styles.formInput}
              value={data.documentNumber}
              onChange={this.handleFormValueChange('documentNumber')}
              name="documentNumber"
            />
          </FormControl>

          <FormControl>
            <FormLabel w="110px">Document Date:</FormLabel>
            <FormDataPicker
              textFieldStyle={styles.formInput}
              name="documentDate"
              formatDate={d => DateTime.fromJSDate(d).toFormat('dd.MM.yyyy')}
              defaultDate={
                data.documentDate ? DateTime.fromSQL(data.documentDate).toJSDate() : undefined
              }
              onChange={this.handleFormValueChange('documentDate')}
              container="inline"
              autoOk
            />
          </FormControl>
          <FormControl>
            <FormLabel w="110px">Delivery Date:</FormLabel>
            <FormDataPicker
              name="deliveryDate"
              textFieldStyle={styles.formInput}
              formatDate={d => DateTime.fromJSDate(d).toFormat('dd.MM.yyyy')}
              defaultDate={
                data.deliveryDate ? DateTime.fromSQL(data.deliveryDate).toJSDate() : undefined
              }
              onChange={this.handleFormValueChange('deliveryDate')}
              container="inline"
              autoOk
            />
          </FormControl>
        </FormGroup>
        <FormGroup w="30%">
          <FormControl>
            <FormLabel w="70px">Reference:</FormLabel>
            <TextField
              textareaStyle={{
                ...styles.formInput,
                width: '100%',
                height: 'calc(100% - 30px)',
              }}
              rows={1}
              rowsMax={10}
              style={{ width: '100%' }}
              multiLine
              value={data.reference}
              onChange={this.handleFormValueChange('reference')}
              name="reference"
            />
          </FormControl>
          <FormControl>
            <FormLabel w="70px">Packages:</FormLabel>
            <FormInputWrap
              inputStyle={styles.formInput}
              value={data.packages}
              onChange={this.handleFormValueChange('packages')}
              name="packages"
            />
          </FormControl>
          <FormControl>
            <FormLabel w="70px">Weight:</FormLabel>
            <FormInputWrap
              inputStyle={styles.formInput}
              value={data.weight}
              onChange={this.handleFormValueChange('weight')}
              name="weight"
            />
          </FormControl>
          <FormControl>
            <FormLabel w="70px">Volume:</FormLabel>
            <FormInputWrap
              inputStyle={styles.formInput}
              value={data.volume}
              onChange={this.handleFormValueChange('volume')}
              name="volume"
            />
          </FormControl>
        </FormGroup>
        <FormGroup w="30%">
          <FormControl>
            <FormLabel w="100px">Delivery terms:</FormLabel>
            <FormSelect
              iconStyle={styles.selectButton}
              labelStyle={styles.formInput}
              name="deliveryTerms"
              value={data.deliveryTerms}
              fullWidth
              menuItemStyle={{ fontSize: '12px' }}
              autoWidth
              onChange={this.handleFormValueChange('deliveryTerms')}
            >
              {
                deliveryTermsOptions.map(term => (
                  <MenuItem key={term} value={term} primaryText={term} />
                ))
              }
            </FormSelect>
          </FormControl>
          <FormControl>
            <FormLabel w="130px">Delivery address:</FormLabel>
            <FormSelect
              iconStyle={styles.selectButton}
              labelStyle={styles.formInput}
              name="deliveryAddress"
              value={data.deliveryAddress}
              fullWidth
              autoWidth
              menuItemStyle={{ fontSize: '12px' }}
              onChange={this.handleFormValueChange('deliveryAddress')}
            >
              {
                deliveryAddressOptions.map(address => (
                  <MenuItem key={address} value={address} primaryText={address} />
                ))
              }
            </FormSelect>
          </FormControl>
          <FormControl>
            <FormLabel w="130px">Payment terms:</FormLabel>
            <FormSelect
              iconStyle={styles.selectButton}
              labelStyle={styles.formInput}
              name="paymentTerms"
              value={data.paymentTerms}
              autoWidth
              fullWidth
              menuItemStyle={{ fontSize: '12px' }}
              onChange={this.handleFormValueChange('paymentTerms')}
            >
              {
                paymentTermOptions.map(term => (
                  <MenuItem key={term} value={term} primaryText={term} />
                ))
              }
            </FormSelect>
          </FormControl>
          <FormControl>
            <FormLabel w="130px">VAT:</FormLabel>
            <FormSelect
              iconStyle={styles.selectButton}
              labelStyle={styles.formInput}
              name="VAT"
              autoWidth
              value={data.VAT}
              fullWidth
              menuItemStyle={{ fontSize: '12px' }}
              onChange={this.handleFormValueChange('VAT')}
            >
              {
                vatOptions.map(vat => (
                  <MenuItem key={vat} value={vat} primaryText={vat} />
                ))
              }
            </FormSelect>
          </FormControl>
        </FormGroup>
        <FormGroup w="30%" style={{ marginRight: '0' }} >
          <FormControl>
            <FormLabel>Notes:</FormLabel>
            <TextField
              textareaStyle={{
                ...styles.formInput,
                width: '100%',
                height: 'calc(100% - 30px)',
              }}
              name="notes"
              rows={1}
              rowsMax={10}
              style={{ width: '100%' }}
              multiLine
              value={data.notes}
              onChange={this.handleFormValueChange('notes')}
            />
          </FormControl>
        </FormGroup>
      </FormRow>
    );
  }
}

CustomerForm.propTypes = {
  data: PropTypes.object.isRequired,
  isCreateForm: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  onSearchCustomer: PropTypes.func.isRequired,
  deliveryTermsOptions: PropTypes.array.isRequired,
  deliveryAddressOptions: PropTypes.array.isRequired,
  paymentTermOptions: PropTypes.array.isRequired,
  vatOptions: PropTypes.array.isRequired,
  searchedCustomers: PropTypes.array.isRequired,
};

CustomerForm.defaultProps = {
  isCreateForm: false,
};

export default CustomerForm;
