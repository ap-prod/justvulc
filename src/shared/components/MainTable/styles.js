import styled from 'styled-components';

export const FormGroup = styled.div`
  margin-bottom: 20px;
  margin-right: 20px;
  ${({ w }) => w && `width: ${w || '100%'}`};
`;

export const FormRow = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
`;

export const FormControl = styled.div`
  display: flex;
  align-items: center;
  padding-right: 10px;
  justify-content: flex-start;
`;

export const ArrowDown = styled.span`
  width: 0; 
  height: 0; 
  border-left: 10px solid transparent;
  border-right: 10px solid transparent;
  border-top: 13px solid  rgb(0, 188, 212);
  margin-left: 10px;
  cursor: pointer;
  display: inline-block;
  margin-bottom: 5px;
`;

export const ErrorCell = styled.div`
  border: 2px solid red;
  margin: -5px;
  padding: 5px;
  height: 54px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: #ffe5e5;
`;

export const AutocompleteItem = styled.div`
  background-color: ${props => props.bgc};
  padding: 3px 15px;
  cursor: pointer;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const styles = {
  menuStyle: {
    borderRadius: '1px',
    border: '1px solid #00bcd4',
    background: '#fff',
    fontSize: '12px',
    position: 'fixed',
    display: 'block',
    width: 'auto%',
    maxWidth: '900px',
    left: 'auto',
    top: 'auto',
    zIndex: 1,
  },
  removeIcon: {
    width: '25px',
    padding: '3px',
    borderRadius: '50%',
    cursor: 'pointer',
    transition: '0.3s',
  },
};
