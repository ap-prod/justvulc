import React from 'react';

// export const TABLE_COLUMNS = [
//   {
//     title: 'Art. No.',
//     propName: 'artNumber',
//   },
//   {
//     title: 'Description',
//     propName: 'description',
//     width: '300px',
//     type: 'MULTY_LINE',
//   },
//   {
//     title: 'Unit',
//     propName: 'unit',
//     type: 'MULTY_LINE',
//     width: '70px',
//   },
//   {
//     title: 'Qty',
//     propName: 'qty',
//   },
//   {
//     title: 'Price',
//     propName: 'price',
//   },
//   {
//     title: 'Pu',
//     propName: 'pu',
//   },
//   {
//     title: 'Total',
//     propName: 'totalMaterials',
//   },
//   {
//     title: 'Net.W',
//     propName: 'netW',
//   },
//   {
//     title: 'Total',
//     propName: 'totalServices',
//   },
//   {
//     title: 'Custom',
//     propName: 'custom',
//   },
// ];

export const POPOVER_TABLE_COLUMNS = [
  {
    title: 'PP',
    propName: 'pp',
  },
  {
    title: <span>P<sub>m</sub></span>,
    propName: 'pm',
  },
  {
    title: <span>P<sub>g</sub></span>,
    propName: 'pg',
  },
  {
    title: 'P',
    propName: 'p',
  },
  {
    title: 'SENT',
    propName: 'sent',
  },
  {
    title: 'AB',
    propName: 'ab',
  },
  {
    title: 'PACK',
    propName: 'pack',
  },
  {
    title: 'WIN',
    propName: 'win',
  },
  {
    title: <span>P<sub>r</sub>/PP</span>,
    propName: 'pr_pp',
  },
  {
    title: <span>P<sub>r</sub>/P<sub>m</sub></span>,
    propName: 'pr_pm',
  },
  {
    title: <span>P<sub>r</sub>/P<sub>g</sub></span>,
    propName: 'pr_pg',
  },
  {
    title: <span>H:H/P<sub>r</sub></span>,
    propName: 'hh_pr',
  },
];
