
import React from 'react';
import { TableHeaderColumn } from 'react-bootstrap-table';
import PropTypes from 'prop-types';
import Autocomplete from 'react-autocomplete';

import { POPOVER_TABLE_COLUMNS } from './popoverTableConfig';
import ADD_ITEM_ROW from './addItemRowMock';
import SubTable from '../SubTable';
import { Icon, Table } from '../../../shared';
import {
  styles,
  ArrowDown,
  ErrorCell,
  AutocompleteItem,
} from './styles';

class MainTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchItemControlValue: '',
      addItemRowData: ADD_ITEM_ROW,
      addItemRowExpanding: [],
    };

    this.onAddItemRowClick = this.onAddItemRowClick.bind(this);
    this.onRowClick = this.onRowClick.bind(this);
    this.createAutoComplete = this.createAutoComplete.bind(this);
    this.handleOpenAutocomplete = this.handleOpenAutocomplete.bind(this);
    this.unitFormatter = this.unitFormatter.bind(this);
    this.descriptionFormatter = this.descriptionFormatter.bind(this);
    this.handleSaveCell = this.handleSaveCell.bind(this);
  }

  componentDidMount() {
    this.keyUpHandler = (e) => {
      this.handleOpenAutocomplete(e);
    };
    document.addEventListener('keyup', this.keyUpHandler);
  }

  componentWillUnmount() {
    document.removeEventListener('keyup', this.keyUpHandler);
  }

  onRowClick(row, columnIndex, rowIndex, e) {
    switch (e.target.nodeName) {
      case 'IMG':
        this.props.onRemoveRow(row);
        break;

      case 'SPAN':
        this.props.onExpandRow(row);
        break;

      default:
        break;
    }
  }

  onAddItemRowClick(row, columnIndex, rowIndex, e) {
    switch (e.target.nodeName) {
      case 'SPAN':
        this.setState({
          addItemRowExpanding: this.state.addItemRowExpanding.length ? [] : [row.id],
        });
        break;

      default:
        break;
    }
  }

  getErrorCellWrapper(v) {
    return <span style={{ fontWeight: 'bold', color: 'red' }}>{ v }</span>;
  }

  addRow(row) {
    this.props.onAddRow(row);
    this.setState({
      searchItemControlValue: '',
      addItemRowData: ADD_ITEM_ROW,
    });
  }

  checkCellForErrors(row, cellName) {
    if (!row.errors) return false;
    return row.id !== 0 ? row.errors.includes(cellName) : false;
  }

  wrapToError(cellName, formatter) {
    return (v, row) => {
      const value = formatter ? formatter(v) : v;

      if (this.checkCellForErrors(row, cellName)) {
        return (
          <ErrorCell>{value}</ErrorCell>
        );
      }
      return value;
    };
  }

  descriptionFormatter(cell, row) {
    const value = cell.map(data => (
      <div
        className="table-cell-row-text"
        key={`${row.artNumber}_${data.label}`}
        title={`${data.label}: ${data.value}`}
      >
        {data.label}: {data.value}
      </div>
    ));
    if (this.checkCellForErrors(row, 'description')) {
      return (
        <ErrorCell>{value}</ErrorCell>
      );
    }
    return <React.Fragment>{value.map(v => v)}</React.Fragment>;
  }

  unitFormatter(cell, row) {
    const value = cell.map(({ measure }, i) => (
      <div
        className="table-cell-row-text"
        key={`${row.artNumber}_${measure}_${i}`} // eslint-disable-line
        title={`${measure}`}
      >
        {measure}
      </div>
    ));
    if (this.checkCellForErrors(row, 'measure')) {
      return (
        <ErrorCell>{value}</ErrorCell>
      );
    }
    return <React.Fragment>{value.map(v => v)}</React.Fragment>;
  }

  handleOpenAutocomplete(e) {
    if (this.state.addItemRowData.artNumber !== null && e.which === 13) {
      this.addRow(this.state.addItemRowData);
    }
  }

  handleSaveCell(...args) {
    this.props.onEditRow(args[0], args[3].rowIndex);
  }

  createAutoComplete(onUpdate) {
    return (
      <div onKeyPress={this.handleOpenAutocomplete}>
        <Autocomplete
          openOnFocus
          open={!!this.state.searchItemControlValue.length}
          items={this.props.searchedItems.slice(0, 10)}
          getItemValue={item => item.artNumber}
          menuStyle={styles.menuStyle}
          renderItem={(item, highlighted) => {
            if (item.artNumber !== this.state.addItemRowData.artNumber && highlighted) {
              setTimeout(() => {
                this.setState({
                  addItemRowData: item,
                });
              }, 0);
            }
            return (
              <AutocompleteItem
                key={item.artNumber}
                bgc={highlighted ? '#ddd' : '#fff'}
              >
                {item.artNumber} -- {
                  item.description.map(({ label, value }, i) => `${i === 0 ? '' : '  '}${value ? (`${label}:`) : ''} ${value || ''}`)
                }
              </AutocompleteItem>
            );
          }}
          value={this.state.searchItemControlValue}
          onChange={(e) => {
            this.props.onSearchArtNumder(e.target.value);
            this.setState({ searchItemControlValue: e.target.value });
          }}
          onSelect={(value, item) => {
            onUpdate(value);
            this.addRow(item);
            this.setState({
              searchItemControlValue: '',
              addItemRowData: ADD_ITEM_ROW,
            });
          }}
        />
      </div>
    );
  }

  render() {
    const { data, expanding } = this.props;
    return (
      <div>
        <Table
          data={data}
          options={{
            expandRowBgColor: 'rgb(0, 188, 212)',
            onRowClick: this.onRowClick,
            expanding,
            expandBy: 'column',
          }}
          cellEdit={{
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.handleSaveCell,
          }}
          striped
          expandableRow={() => true}
          expandComponent={row => <SubTable data={row} columns={POPOVER_TABLE_COLUMNS} />}
          keyField="artNumber"
        >
          <TableHeaderColumn
            dataField="artNumber"
            headerAlign="center"
            dataAlign="center"
            width="100px"
            editable={false}
            expandable={false}
          >
            Art. No.
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="description"
            dataFormat={this.descriptionFormatter}
            width="500px"
            headerAlign="center"
            editable={false}
            expandable={false}
          >
            Description
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="description"
            dataFormat={this.unitFormatter}
            headerAlign="center"
            editable={false}
            expandable={false}
          >
            Unit
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="qty"
            headerAlign="center"
            editColumnClassName="class-for-editing-cell"
            dataAlign="center"
            expandable={false}
            dataFormat={this.wrapToError('qty')}
          >
            Qty
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="price"
            headerAlign="center"
            editColumnClassName="class-for-editing-cell"
            dataAlign="center"
            dataFormat={this.wrapToError('price')}
            expandable={false}
          >
            Price
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="pu"
            headerAlign="center"
            editable={false}
            expandable={false}
            dataAlign="center"
            dataFormat={this.wrapToError('pu')}
          >
            Pu
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="unit"
            headerAlign="center"
            dataAlign="center"
            editable={false}
            expandable={false}
            dataFormat={(...args) => {
              const { qty, price, pu } = args[1];
              return ((qty * price) / pu).toFixed(2);
            }}
          >
            Sum
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="netW"
            headerAlign="center"
            editColumnClassName="class-for-editing-cell"
            expandable={false}
            dataAlign="center"
            dataFormat={this.wrapToError('netW', v => Number(v).toFixed(3))}
          >
            Net.W
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="unit"
            headerAlign="center"
            editColumnClassName="class-for-editing-cell"
            dataAlign="center"
            editable={false}
            expandable={false}
            dataFormat={(...args) => {
              const { netW, qty } = args[1];
              return (qty * netW).toFixed(3);
            }}
          >
            Total Weight
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="custom"
            headerAlign="center"
            editColumnClassName="class-for-editing-cell"
            dataAlign="center"
            expandable={false}
          >
            Custom
          </TableHeaderColumn>
          <TableHeaderColumn
            editable={false}
            headerAlign="center"
            expandable={false}
            dataFormat={() => (
              <span>
                <Icon
                  style={styles.removeIcon}
                  icon="close"
                  hoveredStyle={{
                    backgroundColor: 'rgb(187, 222, 251)',
                  }}
                  title="Remove"
                />
                <ArrowDown title="Expand" />
              </span>
            )}
            dataAlign="center"
          >
            Actions
          </TableHeaderColumn>
        </Table>
        <Table
          data={[this.state.addItemRowData]}
          options={{
            expandRowBgColor: 'rgb(0, 188, 212)',
            onRowClick: this.onAddItemRowClick,
            expanding: this.state.addItemRowExpanding,
            expandBy: 'column',
          }}
          cellEdit={{
            mode: 'click',
            blurToSave: true,
          }}
          striped
          expandableRow={() => true}
          expandComponent={row => <SubTable data={row} columns={POPOVER_TABLE_COLUMNS} />}
          keyField="id"
        >
          <TableHeaderColumn
            dataField="artNumber"
            dataAlign="center"
            width="100px"
            editColumnClassName="class-for-editing-cell"
            customEditor={{ getElement: this.createAutoComplete }}
            className="col-hidden"
            expandable={false}
          >
            .
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="description"
            editable={false}
            dataFormat={this.descriptionFormatter}
            width="500px"
            className="col-hidden"
            expandable={false}
          >
            .
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="description"
            editable={false}
            className="col-hidden"
            dataFormat={(...args) => {
              if (args[1].artNumber !== null) {
                return this.unitFormatter(...args);
              }
              return null;
            }}
            expandable={false}
          >
            .
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="qty"
            dataAlign="center"
            className="col-hidden"
            editable={false}
            expandable={false}
          >
            .
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="price"
            dataAlign="center"
            className="col-hidden"
            editable={false}
            expandable={false}
          >
            .
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="pu"
            editable={false}
            className="col-hidden"
            expandable={false}
            dataAlign="center"
          >
            .
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="unit"
            dataAlign="center"
            className="col-hidden"
            editable={false}
            expandable={false}
          >
            .
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="netW"
            editable={false}
            expandable={false}
            dataAlign="center"
            className="col-hidden"
          >
            .
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="unit"
            editable={false}
            className="col-hidden"
            dataAlign="center"
            expandable={false}
          >
            .
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="custom"
            dataAlign="center"
            editable={false}
            className="col-hidden"
            expandable={false}
          >
            .
          </TableHeaderColumn>
          <TableHeaderColumn
            editable={false}
            className="col-hidden"
            headerAlign="center"
            expandable={false}
            dataFormat={(...args) => {
              if (args[1].artNumber === null) {
                return null;
              }
              return (
                <ArrowDown title="Expand" />
              );
            }}
            dataAlign="center"
          >
          .
          </TableHeaderColumn>
        </Table>
      </div>
    );
  }
}

MainTable.propTypes = {
  data: PropTypes.array.isRequired,
  expanding: PropTypes.array.isRequired,
  onRemoveRow: PropTypes.func.isRequired,
  onExpandRow: PropTypes.func.isRequired,
  onAddRow: PropTypes.func.isRequired,
  onEditRow: PropTypes.func.isRequired,
  onSearchArtNumder: PropTypes.func.isRequired,
  searchedItems: PropTypes.array.isRequired,
};

export default MainTable;
