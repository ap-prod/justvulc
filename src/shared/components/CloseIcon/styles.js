import styled from 'styled-components';

export const CloseIcon = styled.span`
  font-size: 30px;
  float: right;
  cursor: pointer
`;
