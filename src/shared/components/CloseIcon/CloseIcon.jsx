import React from 'react';
import { CloseIcon } from './styles';

const Icon = props => <CloseIcon {...props}>&#xd7;</CloseIcon>;

export default Icon;
