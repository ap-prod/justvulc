import styled from 'styled-components';
import { BootstrapTable } from 'react-bootstrap-table';

export const Clickable = styled.span`
  cursor: pointer;
`;

export const StyledTable = styled(BootstrapTable)`
  & .my-header-class,
  & .my-footer-class{
    width: 100%;
    border-collapse: collapse;
  }
  & .my-header-class th,
  & .my-footer-class th{
    width: 100%;
    padding: 10px 5px;
    border-collapse: collapse;
    background-color: rgb(187, 222, 251);
  }
  & .my-footer-class th{
    border: 1px solid rgb(224, 224, 224);
  }
  & .my-body-class{
    width: 100%;
    border-collapse: collapse;
  }
  & .my-body-class td{
    border: 1px solid rgb(224, 224, 224);
    padding: 5px;
  }
  .strocked td{
      padding: 0 5px !important;
  }
  &.my-container-class{
    width: 100%;
  }
  &.my-container-class .react-bs-table-bordered{
    border-bottom: 0;
  }
  &.my-container-summ-class .react-bs-table-bordered{
    border-top: 0;
  }
  &.my-container-summ-class .my-body-class td{
    border-top: none;
    height: 30px;
  }
  &.my-container-summ-class .my-body-class td:first-child{
    cursor: text;
  }
  & .table-cell-row-text{
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    padding: 2px 0;
  }
  & .react-bs-table .react-bs-table td {
    background-color: #FFF;
  }
  & .class-for-editing-cell input{
    width: calc(100% - 5px);
    padding: 0px;
    position: relative;
    border: none;
    outline: none;
    background-color: rgba(0, 0, 0, 0);
    color: rgba(0, 0, 0, 0.87);
    cursor: inherit;
    font-style: inherit;
    font-variant: inherit;
    font-weight: inherit;
    font-stretch: inherit;
    font-size: 12px;
    line-height: inherit;
    font-family: inherit;
    opacity: 1;
    height: 30px;
    border-bottom: 2px solid rgb(0, 188, 212);
  }
  & .react-bs-table .react-bs-container-footer>table>thead>tr>th .filter,
  & .react-bs-table .react-bs-container-header>table>thead>tr>th .filter,
  & .react-bs-table .react-bs-container-header>table>thead>tr>th .number-filter-input,
  & .react-bs-table .react-bs-container-header>table>thead>tr>th .number-filter-comparator {
    width: 100%;
    height: 30px;
    padding: 0 15px;
    box-sizing: border-box;
  }
  & .react-bs-table .react-bs-container-header>table>thead>tr>th .number-filter-comparator {
    width: 60px;
  }
  & .react-bs-table th,
  & .react-bs-table td{
    font: inherit;
  }
  & .react-bs-table .text-filter:focus,
  & .react-bs-table .number-filter-input:focus,
  & .react-bs-table .number-filter-comparator:focus,
  & .text-filter:hover {
    width: 100%;
    background-color: #E0F2F1;
  }
  & .react-bs-table .text-filter,
  & .react-bs-table .number-filter-comparator,
  & .react-bs-table .number-filter-input {
    font: inherit;
    font-style: normal;
    font-size: 12px;
    width: 100px;
    height: 20px;
    box-sizing: border-box;
    border: 0;
    margin: 3px 0 0 0;
    padding: 8px 8px 8px 52px;
    display: inline-block;
    background: none;
    white-space: normal;
    vertical-align: middle;
    color: #000;
    background-color: #E1F5FE;
    transition: .3s;
    outline: none;
  }
  & .col-hidden{
    display: none;
  }
  .react-bootstrap-table-page-btns-ul.pagination {
    list-style: none;
    float: right;
    margin-top: 10px;
    display: flex;
    flex-direction: row;
  }
  .react-bootstrap-table-page-btns-ul.pagination>.page-item {
    padding: 10px 10px 3px 10px;
    font-size: 16px;
    border-bottom: 2px solid transparent;
    &.active {
      border-bottom: 2px solid rgb(0, 188, 212);
    }
    & a {
      color: inherit;
      text-decoration: none;
    }
  }
  .row {
    display: flex;
  }
  .col-md-6 {
    width: 50%;
  }
`;

