import React from 'react';
import PropTypes from 'prop-types';
import { StyledTable } from './styles';

const Table = props => (
  <StyledTable
    {...props}
    tableHeaderClass="my-header-class"
    tableBodyClass="my-body-class"
    containerClass="my-container-class my-container-summ-class"
    tableFooterClass="my-footer-class"
  >
    {props.children}
  </StyledTable>
);

Table.propTypes = {
  children: PropTypes.array,
};

Table.defaultProps = {
  children: [],
};

export default Table;
