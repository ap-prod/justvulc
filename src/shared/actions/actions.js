import { createAction } from '../../utils';

export const logoutUser = createAction('LOGOUT_REQUESTED');

export const addMaterialsAndServicesRow = createAction('ADD_MATERIAL_AND_SERVICES_ROW');
export const editMaterialsAndServicesRow = createAction('EDIT_MATERIAL_AND_SERVICES_ROW');
export const fetchItemsByArtNumber = createAction('ITEMS_BY_ART_NUMBER_FETCH_REQUESTED');
export const searchCustomer = createAction('CUSTOMERS_SEARCH_FETCH_REQUESTED');
export const clearSearchCustomer = createAction('CLEAR_SEARCHED_CUSTOMERS');
export const changeCustomerData = createAction('CHANGE_CUSTOMER_DATA');
export const removeMaterialsAndServicesRow = createAction('REMOVE_MATERIAL_AND_SERVICES_ROW');
export const closeEditModal = createAction('CLOSE_CREATE_CARD_MODAL');
export const saveEditedCard = createAction('SAVE_EDITED_CARD_REQUESTED');
export const createCard = createAction('CREATE_CARD_REQUESTED');
export const saveMovedCard = createAction('SAVE_MOVED_CARD_REQUESTED');
export const fetchEntry = createAction('ENTRY_DATA_FETCH_REQUESTED');
export const fetchMovedCardsEntryData = createAction('ENTRIES_DATA_FETCH_REQUESTED');
export const fetchMoveEntryData = createAction('MOVE_ENTRY_DATA_FETCH_REQUESTED');
export const openStockWtabCardModal = createAction('OPEN_STOCK_WTAB_CARD_MODAL');
