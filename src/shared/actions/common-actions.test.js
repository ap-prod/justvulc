import {
  logoutUser,
} from './';

describe('Commmon action creators', () => {
  it('should return request logout action', () => {
    const payloadMock = [{ cookie: 'test', login: 'Test' }];
    const action = logoutUser(payloadMock);
    expect(action.type).toBe('LOGOUT_REQUESTED');
    expect(action.payload).toBe(payloadMock);
  });
});
