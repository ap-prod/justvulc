import { call, put, takeEvery } from 'redux-saga/effects';
import Api from '../../dashboard/services/api';

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* fetchUser(action) {
  try {
    const res = yield call(Api.fetchUser, action.payload);
    yield put({ type: 'USER_DATA_FETCH_SUCCEEDED', payload: res.data });
  } catch (e) {
    yield put({ type: 'USER_DATA_FETCH_FAILED', error: e });
  }
}

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* logoutUser(action) {
  try {
    const res = yield call(Api.logoutUser, action.payload);
    yield put({ type: 'LOGOUT_SUCCEEDED', payload: res.data });
  } catch (e) {
    yield put({ type: 'LOGOUT_FAILED', error: e });
  }
}

/*
 * Starts fetchBoard on each dispatched `BOARD_DATA_FETCH_REQUESTED` action.
 */
function* sharedSaga() {
  yield takeEvery('USER_DATA_FETCH_REQUESTED', fetchUser);
  yield takeEvery('LOGOUT_REQUESTED', logoutUser);
}

export default sharedSaga;
